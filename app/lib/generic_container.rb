# frozen_string_literal: true

# A utility class to provide a unified interface for running both security containers and utility
# containers (service discovery, connectivity checks, etc)
class GenericContainer
  attr_reader :full_path

  def initialize(opts = {})
    @security_container = SecurityContainer.find(opts[:security_container_id]) if opts[:security_container_id]
    @full_path = opts[:full_path] || @security_container&.full_path
    raise 'A full_path or security_container_id must be provided' unless full_path
  end

  def official?
    return true unless @security_container
    @security_container.official?
  end

  def creds
    return {} if official?
    repository = @security_container&.security_test_repository
    return {} if repository&.username.blank?
    { 'Username' => repository.username, 'Password' => repository.password }
  end
end
