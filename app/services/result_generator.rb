# frozen_string_literal: true

class ResultGenerator
  attr_reader :assessment

  class << self
    def placeholder_signatures
      [api_result_error_signature, relay_result_error_signature, result_timeout_signature]
    end

    def api_result_error_signature
      hexdigest common_error_attrs.merge(output: Result::API_ERROR_OUTPUT).to_json
    end

    def relay_result_error_signature
      hexdigest common_error_attrs.merge(output: Result::RELAY_ERROR_OUTPUT).to_json
    end

    def result_timeout_signature
      hexdigest common_attrs.merge(output: Result::ASSESSMENT_TIMEOUT_OUTPUT, nid: 'norad:timeout').to_json
    end

    def result_timeout_attrs
      common_attrs.merge(
        nid: 'norad:timeout',
        output: Result::ASSESSMENT_TIMEOUT_OUTPUT,
        title: Result::ASSESSMENT_TIMEOUT_TITLE,
        signature: result_timeout_signature
      )
    end

    def api_result_error_attrs
      common_error_attrs.merge(
        title: Result::API_ERROR_TITLE,
        output: Result::API_ERROR_OUTPUT,
        signature: api_result_error_signature
      )
    end

    def relay_result_error_attrs
      common_error_attrs.merge(
        title: Result::RELAY_ERROR_TITLE,
        output: Result::RELAY_ERROR_OUTPUT,
        signature: relay_result_error_signature
      )
    end

    private

    def hexdigest(data)
      OpenSSL::Digest::SHA256.hexdigest(data)
    end

    def common_error_attrs
      common_attrs.merge(nid: 'norad:error')
    end

    def common_attrs
      { sir: 'no_impact', status: :error }
    end
  end

  def initialize(assessment)
    @assessment = assessment
  end

  def create_errored_api_result!
    create_result!(self.class.api_result_error_attrs)
  end

  def create_errored_relay_result!
    create_result!(self.class.relay_result_error_attrs)
  end

  def create_timed_out_result!
    create_result!(self.class.result_timeout_attrs)
  end

  private

  def create_result!(attrs)
    ActiveRecord::Base.transaction do
      assessment.results.create! attrs

      # Calling assessment.complete! twice in a transaction results in a race condition
      # where Postgres fails to properly update the state and state_transition_time and results
      # in inconsistent values for those fields.
      # This particular scenario happens when the assessments/update endpoint is hit, so avoid that here.
      assessment.complete! unless assessment.complete?
    end
  end
end
