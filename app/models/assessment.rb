# frozen_string_literal: true

# == Schema Information
#
# Table name: assessments
#
#  id                           :integer          not null, primary key
#  identifier                   :string           not null
#  machine_id                   :integer
#  state                        :integer          default("pending_scheduling"), not null
#  title                        :string
#  description                  :string
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  security_container_id        :integer
#  security_container_secret_id :integer
#  docker_command_id            :integer
#  state_transition_time        :datetime         not null
#  service_id                   :integer
#  type                         :string           not null
#
# Indexes
#
#  index_assessments_on_docker_command_id             (docker_command_id)
#  index_assessments_on_identifier                    (identifier) UNIQUE
#  index_assessments_on_machine_id                    (machine_id)
#  index_assessments_on_security_container_id         (security_container_id)
#  index_assessments_on_security_container_secret_id  (security_container_secret_id)
#  index_assessments_on_service_id                    (service_id)
#
# Foreign Keys
#
#  fk_rails_1da80f5b8d  (security_container_secret_id => security_container_secrets.id) ON DELETE => nullify
#  fk_rails_1e655192d7  (docker_command_id => docker_commands.id) ON DELETE => cascade
#  fk_rails_85b2eca077  (security_container_id => security_containers.id) ON DELETE => cascade
#  fk_rails_d67eacf5ca  (service_id => services.id) ON DELETE => cascade
#  fk_rails_f44c8209a6  (machine_id => machines.id) ON DELETE => cascade
#

class Assessment < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # State Machine
  include AssessmentStateMachine

  SHA1_HEX_LENGTH = 40
  RANKED_ASSESSMENT_STATUSES = %i[erroring failing warning passing informing].freeze
  STALE_HOUR = 24

  # Attribute information
  def to_param
    identifier
  end
  attr_readonly :identifier

  # Associations
  has_many :results, inverse_of: :assessment
  belongs_to :docker_command, inverse_of: :assessments
  belongs_to :machine, inverse_of: :assessments
  belongs_to :service, inverse_of: :assessments
  belongs_to :security_container, inverse_of: :assessments
  belongs_to :security_container_secret, inverse_of: :assessments

  # Validations
  validates :identifier, format: { with: /\A[a-f0-9]{40}\z/, message: 'must be a SHA1 hash' }, allow_blank: true
  validates :docker_command, presence: true
  validates :machine, presence: true
  validates :service, presence: :true, unless: proc { |a| a.security_container&.whole_host? }
  validates :security_container, presence: true
  validates :security_container_secret, presence: true, unless: 'complete?'

  # Callback Declarations
  # This could result in the state transition
  before_create :initialize_state_transition_timestamp
  around_create :generate_identifier

  class << self
    def find(input)
      return super unless input.to_s.length == SHA1_HEX_LENGTH
      find_by(identifier: input).tap { |assessment| raise ActiveRecord::RecordNotFound unless assessment }
    end

    def latest
      command = DockerCommand.where(id: select(:docker_command_id).distinct).order(:created_at).last
      command ? where(docker_command: command) : none
    end

    def latest_results_stats
      Result.where(assessment: latest).stats
    end

    def stale
      where('state_transition_time <= ?', STALE_HOUR.hours.ago).where.not(state: :complete)
    end

    def most_recent_by_docker_command_id(limit = 5)
      for_docker_command(
        select(:docker_command_id).order(docker_command_id: :desc).distinct.limit(limit)
      ).order(docker_command_id: :desc)
    end

    def for_docker_command(dcid)
      where(docker_command_id: dcid)
    end

    # Status from a single assessment_stats based on severity
    def status_from_stats(stats)
      RANKED_ASSESSMENT_STATUSES.detect(-> { :pending }) { |status| stats[status]&.positive? }
    end
  end

  # This class size is getting large. The contents of this method are a candidate for being moved to
  # a service object.
  def create_result_set!(results_array)
    transaction do
      remove_placeholder_results!
      results_array.each do |result_attrs|
        results.safe_create result_attrs
      end
      complete!
      mark_ignored_results_as_noise
    end
  end

  def timeout!
    ResultGenerator.new(self).create_timed_out_result!
  end

  # This class size is getting large. The contents of this method are a candidate for being moved to
  # a query object.
  def associated_requirements
    return [] unless security_container
    security_container
      .requirements
      .joins(requirement_group: :organizations)
      .where(organizations: { id: machine.organization_id })
  end

  def latest_results
    results
  end

  def results_path
    Rails.application.routes.url_helpers.v1_assessment_results_path identifier
  end

  private

  def remove_placeholder_results!
    results.with_signature(ResultGenerator.placeholder_signatures).destroy_all
  end

  # Callback Definition
  def generate_identifier
    self.identifier = OpenSSL::Digest::SHA1.hexdigest("#{Time.now.to_f}#{SecureRandom.hex}")
    yield
  rescue ActiveRecord::RecordNotUnique
    retry
  end

  def initialize_state_transition_timestamp
    self.state_transition_time = created_at
  end

  def mark_ignored_results_as_noise
    ignore_query = ResultIgnoreRule.select(:signature).for_ignore_scopes(machine, machine.organization)
    IgnoreRuleApplicator.new(ignore_query, self).apply
  end
end
