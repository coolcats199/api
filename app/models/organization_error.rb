# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

class OrganizationError < ApplicationRecord
  belongs_to :errable, polymorphic: true, required: false
  belongs_to :organization, required: true

  class << self
    def create_error(org, errable = nil)
      where(type: name, organization: org, errable: errable).first_or_create!
    end

    def remove_error(org, errable = nil)
      if errable
        errable.organization_errors.where(type: name).destroy_all
      else
        org.organization_errors.where(type: name).destroy_all
      end
    end
  end
end
