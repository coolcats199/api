# frozen_string_literal: true

# == Schema Information
#
# Table name: memberships
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_memberships_on_organization_id              (organization_id)
#  index_memberships_on_user_id                      (user_id)
#  index_memberships_on_user_id_and_organization_id  (user_id,organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_64267aab58  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_99326fb65d  (user_id => users.id) ON DELETE => cascade
#

class Membership < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Associations
  belongs_to :user
  belongs_to :organization

  validates :user, uniqueness: { scope: :organization_id }

  def self.create_admin!(user, organization)
    membership = new(user_id: user.id, organization_id: organization.id)
    return membership unless membership.valid?

    Membership.transaction do
      membership.save!
      user.add_role :organization_admin, organization
    end

    membership
  end

  def self.create_reader!(user, organization)
    membership = new(user_id: user.id, organization_id: organization.id)
    return membership unless membership.valid?

    Membership.transaction do
      membership.save!
      user.add_role :organization_reader, organization
    end

    membership
  end

  def destroy_with_roles!
    Membership.transaction do
      determine_roles.each do |role|
        user.remove_role role, organization
      end
      destroy!
    end
  end

  def role
    user.roles.find_by(resource: organization)
  end

  private

  def determine_roles
    user.roles.each_with_object([]) do |role, a|
      a << role.name.to_sym if organization.id == role.resource_id && role.resource_type == 'Organization'
    end
  end
end
