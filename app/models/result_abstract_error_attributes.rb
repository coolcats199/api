# frozen_string_literal: true

# This is an abstract class that helps implement the Template design
# pattern. This class should not be instantiated, but classes that
# inherit from this class are specific instances of this template.
# Classes inheriting from this class need to implement the following
# methods: generate_title, generate_description, and generate_nid

class ResultAbstractErrorAttributes
  attr_reader :title, :description, :status, :nid, :signature

  def initialize(exception)
    @title = generate_title
    @description = generate_description(exception)
    @status = :error
    @nid = generate_nid
    # The signature should be unique for each Error Result
    @signature = SecureRandom.hex(32)
  end

  def to_h
    {
      title: @title,
      description: @description,
      status: @status,
      nid: @nid,
      signature: @signature
    }
  end

  private

  def generate_title
    raise NotImplementedError
  end

  def generate_description(_exception)
    raise NotImplementedError
  end

  def generate_nid
    raise NotImplementedError
  end
end
