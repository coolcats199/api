# frozen_string_literal: true

# == Schema Information
#
# Table name: machine_connectivity_checks
#
#  id                           :integer          not null, primary key
#  machine_id                   :integer          not null
#  security_container_secret_id :integer
#  finished_at                  :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  status                       :integer
#  type                         :string           not null
#  status_reason                :text
#
# Indexes
#
#  index_machine_connectivity_checks_on_finished_at          (finished_at)
#  index_machine_connectivity_checks_on_machine_id_and_type  (machine_id,type)
#  index_machine_connectivity_checks_on_s_c_s_id             (security_container_secret_id) UNIQUE
#  index_machine_connectivity_checks_on_status               (status)
#  index_machine_connectivity_checks_on_updated_at           (updated_at)
#
# Foreign Keys
#
#  fk_rails_74009af901  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7a47e669da  (security_container_secret_id => security_container_secrets.id) ON DELETE => nullify
#

class MachineConnectivityCheck < ApplicationRecord
  # Adds ::expired scope
  include Perishable

  # RBAC
  include Authority::Abilities

  MAX_DURATION = 10.minutes
  TIMED_OUT_STATUS_REASON = 'The connectivity check did not succeed within 10 minutes.'

  # Associations
  belongs_to :machine
  belongs_to :security_container_secret

  enum status: %i[passing failing]

  # Query Scopes
  scope :stale, -> { where(finished_at: nil, status: nil).where('updated_at < ?', MAX_DURATION.ago.utc) }

  # Callbacks
  # Use after_commit to ensure the check is finished regardless of whether the error gets created
  after_commit :create_error, if: -> { failing? && latest? }
  after_commit :remove_error, if: -> { passing? && latest? }

  validate :eligible, on: :create

  def self.latest!
    order(:created_at).last!
  end

  def shared_secret
    security_container_secret&.secret
  end

  def latest?
    !self.class.where('created_at > ?', created_at).where(machine: machine).exists?
  end

  private

  # By default, all checks are eligible to run
  def eligible
    true
  end

  def create_error
    self.class::ORG_ERROR_KLASS.constantize.create_error(machine.organization, machine)
  end

  def remove_error
    self.class::ORG_ERROR_KLASS.constantize.remove_error(machine.organization, machine)
  end
end
