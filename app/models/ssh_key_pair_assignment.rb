# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pair_assignments
#
#  id              :integer          not null, primary key
#  machine_id      :integer          not null
#  ssh_key_pair_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_ssh_key_pair_assignments_on_machine_id       (machine_id) UNIQUE
#  index_ssh_key_pair_assignments_on_ssh_key_pair_id  (ssh_key_pair_id)
#
# Foreign Keys
#
#  fk_rails_05e7648bab  (ssh_key_pair_id => ssh_key_pairs.id) ON DELETE => cascade
#  fk_rails_7c3731b704  (machine_id => machines.id) ON DELETE => cascade
#

class SshKeyPairAssignment < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Validations
  validates :machine, presence: true
  validates :ssh_key_pair, presence: true
  validate :keypair_and_machine_in_same_organization

  # Associations
  belongs_to :machine
  belongs_to :ssh_key_pair

  include OrganizationErrorWatcher
  watch_for_organization_errors NoSshKeyPairAssignmentError,
                                KeyPairAssignmentWithoutSshServiceError,
                                SshServiceWithoutKeyPairAssignmentError,
                                on: %i[create destroy],
                                organization_method: :resolved_organization,
                                subject_method: :machine
  watch_for_organization_errors UnableToSshToMachineError,
                                organization_method: :resolved_organization,
                                subject_method: :machine,
                                on: :create,
                                unless: -> { machine.organization.configuration.use_relay_ssh_key }
  destroy_associated_organization_errors UnableToSshToMachineError,
                                         subject_method: :machine,
                                         unless: -> { machine.organization.configuration.use_relay_ssh_key }

  private

  def keypair_and_machine_in_same_organization
    return if machine&.organization_id == ssh_key_pair&.organization_id
    errors.add(:base, 'Key Pair and Machine must belong to the same organization')
  end

  def resolved_organization
    machine.organization
  end
end
