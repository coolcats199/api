# frozen_string_literal: true

# == Schema Information
#
# Table name: authentication_methods
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_authentication_methods_on_type     (type)
#  index_authentication_methods_on_user_id  (user_id) UNIQUE
#

class ReverseProxyAuthenticationMethod < AuthenticationMethod
  # Callbacks
  after_create -> { user.send_welcome_email }

  def authenticate(*)
    # by this point we've already created and found the user in the DB
    user
  end
end
