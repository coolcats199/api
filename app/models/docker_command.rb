# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#

require './lib/ip_utils'

class DockerCommand < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # State Machine
  include DockerCommandStateMachine

  # Associations
  acts_as_poly :commandable, :machine, :organization, inverse_of: :docker_commands
  has_many :assessments, inverse_of: :docker_command
  has_many :scan_containers, inverse_of: :docker_command
  has_many :security_containers, through: :scan_containers, inverse_of: :docker_commands

  accepts_nested_attributes_for :scan_containers

  # Validations
  validates :machine, presence: true, if: proc { |a| a.organization.blank? }
  validates :organization, presence: true, if: proc { |a| a.machine.blank? }
  validates :scan_containers, presence: true
  validate :relay_is_configured, if: :unreachable_machine?, on: :create

  # Callback Declarations
  before_destroy :check_if_scan_complete
  after_commit :queue_build_assesments_job, on: :create

  def self.most_recent
    order(:created_at).last
  end

  def increment_assessments_in_flight
    increment! :assessments_in_flight
  end

  def decrement_assessments_in_flight
    decrement! :assessments_in_flight
    # <=0 is used to handle the case where a ScheduleContainerJob starts and fails before assessment.start! is called
    post_process_completed_scan if assessments_in_flight <= 0
    assessments_in_flight
  end

  def scanned_machine_count
    Machine.where(id: assessments.pluck(:machine_id)).count
  end

  def selected_machine_count
    organization&.machines&.count || 1
  end

  # always get an organization
  def resolved_organization
    organization || machine&.organization
  end

  private

  def unreachable_machine?
    IpUtils.rfc1918_relay_required? && resolved_organization&.unreachable_machine?
  end

  # Callback Definitions
  def queue_build_assesments_job
    AssessmentBuilderJob.perform_later self
  end

  # Validation Definitions
  def relay_is_configured
    return true if resolved_organization.docker_relay_queue
    errors.add :base, "Machines can't contain RFC1918 IP addresses when no Relay configured"
    throw :abort
  end

  def check_if_scan_complete
    return true if finished_at.present?
    errors.add(:base, 'Scan must be complete before removing')
    throw :abort
  end

  def post_process_completed_scan
    NotificationService.scan_complete(self)
    resolved_organization.auto_export_results(self)
  end
end
