# frozen_string_literal: true

# == Schema Information
#
# Table name: result_export_queues
#
#  id              :integer          not null, primary key
#  organization_id :integer          not null
#  created_by      :string           not null
#  type            :string           not null
#  auto_sync       :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_result_export_queues_on_organization_id           (organization_id)
#  index_result_export_queues_on_organization_id_and_type  (organization_id,type) UNIQUE WHERE ((type)::text = 'InfosecExportQueue'::text)
#  index_result_export_queues_on_type                      (type)
#
# Foreign Keys
#
#  fk_rails_e5fc28c9ec  (organization_id => organizations.id) ON DELETE => cascade
#

class InfosecExportQueue < ResultExportQueue
  # RBAC
  self.authorizer_name = "#{superclass.name}Authorizer"

  has_one :infosec_export_queue_configuration, foreign_key: :result_export_queue_id, inverse_of: :infosec_export_queue
  validates :infosec_export_queue_configuration, presence: true
  accepts_nested_attributes_for :infosec_export_queue_configuration # destroy handled at the db level

  def amqp_queue_name
    'result_export_queue.cisco_infosec.21f4a6b84612b4a40d5974dcdd9e4106'
  end

  def target_connection_settings
    super.merge(settings: infosec_export_queue_configuration.attributes)
  end
end
