# frozen_string_literal: true

# == Schema Information
#
# Table name: web_application_configs
#
#  id                             :integer          not null, primary key
#  auth_type                      :integer          default("unauthenticated"), not null
#  url_blacklist                  :string
#  starting_page_path             :string           default("/"), not null
#  login_form_username_field_name :string
#  login_form_password_field_name :string
#  service_id                     :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#
# Indexes
#
#  index_web_application_configs_on_service_id  (service_id)
#
# Foreign Keys
#
#  fk_rails_048e8c2543  (service_id => services.id) ON DELETE => cascade
#

class WebApplicationConfig < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Attribute Information
  enum auth_type: { unauthenticated: 0, form: 1, digest: 2, basic: 3 }

  # Validations
  validates :web_application_service, presence: true
  validates :starting_page_path, format: { with: %r{\A/\S*}, message: 'must be a valid path' }

  # Associatons
  belongs_to :web_application_service, foreign_key: :service_id

  # Callbacks
  before_validation do
    self.starting_page_path = '/' if starting_page_path.blank?
    self.auth_type = :unauthenticated if auth_type.blank?
    self.url_blacklist = '' if url_blacklist.nil? # empty string is OK
    if auth_type == 'form'
      self.login_form_username_field_name = 'username' if login_form_username_field_name.blank?
      self.login_form_password_field_name = 'password' if login_form_password_field_name.blank?
    end
  end
end
