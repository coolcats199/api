# frozen_string_literal: true

# == Schema Information
#
# Table name: infosec_export_queue_configurations
#
#  id                     :integer          not null, primary key
#  ctsm_id                :string
#  result_export_queue_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_ieq_configs_on_result_export_queue_id  (result_export_queue_id)
#
# Foreign Keys
#
#  fk_rails_e9e9747506  (result_export_queue_id => result_export_queues.id) ON DELETE => cascade
#

class InfosecExportQueueConfiguration < ApplicationRecord
  # RBAC
  include Authority::Abilities

  belongs_to(
    :infosec_export_queue,
    foreign_key: :result_export_queue_id,
    inverse_of: :infosec_export_queue_configuration
  )

  validates(
    :ctsm_id,
    format: {
      with: /\ACTSM-\d+\z/,
      message: 'has to start with "CTSM-" and end with a number'
    },
    allow_blank: true
  )
end
