# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  official           :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host) UNIQUE
#  index_security_test_repositories_on_name  (name) UNIQUE
#

class SecurityTestRepository < ApplicationRecord
  # Prevent Official Repo Changes
  include RepositoryImmutable

  # Used to bypass official check
  attr_accessor :allow_official_changes

  # Encrypted Attributes
  include Vault::EncryptedModel
  vault_attribute :password

  # RBAC
  include Authority::Abilities
  resourcify

  # Associations
  has_many :security_containers
  has_many :repository_whitelist_entries, dependent: :destroy
  has_many :repository_members

  # Validations
  validates :password, presence: true, if: 'username.present?'
  validates :name, presence: true
  validate :prevent_official_changes, unless: :allow_official_changes

  # Callbacks
  before_destroy :prevent_official_changes, unless: :allow_official_changes
  before_save -> { host && host.gsub!(%r{/{1,}\z}, '') } # chomp slashes off host:port

  # mimic the behavior of save
  def save_with_admin(user)
    transaction do
      save!
      RepositoryMember.create!(role_type: :admin, user: user, security_test_repository: self)
      true
    end
  rescue ActiveRecord::RecordInvalid
    false
  end
end
