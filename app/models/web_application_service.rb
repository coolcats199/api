# frozen_string_literal: true

# == Schema Information
#
# Table name: services
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  description         :text
#  port                :integer          not null
#  port_type           :integer          default("tcp"), not null
#  encryption_type     :integer          default("cleartext"), not null
#  machine_id          :integer
#  type                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  allow_brute_force   :boolean          default(FALSE), not null
#  application_type_id :integer
#  discovered          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_application_type_id  (application_type_id)
#  index_services_on_machine_id           (machine_id)
#  index_services_on_machine_id_and_port  (machine_id,port) UNIQUE
#  index_services_on_type                 (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (application_type_id => application_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#

class WebApplicationService < Service
  COMMON_WEB_PORTS = [80, 443, 8080, 8443].freeze

  validate :encryption_type_is_ssl_or_disabled
  validate :port_type_is_tcp

  before_validation :set_service_defaults

  has_one :web_application_config, foreign_key: :service_id
  accepts_nested_attributes_for :web_application_config, reject_if: :all_blank

  after_initialize do |service|
    service.web_application_config&.web_application_service = service
  end

  def service_configuration
    super.merge(web_application_config_options)
  end

  def serializer_klass
    WebApplicationServiceSerializer
  end

  private

  # Validation Definitions
  def encryption_type_is_ssl_or_disabled
    errors.add(:encryption_type, 'must be SSL or cleartext') unless ssl? || cleartext?
  end

  def port_type_is_tcp
    errors.add(:port_type, 'must be TCP') unless tcp?
  end

  # Callback Definitions
  def set_service_defaults
    self.port_type = :tcp # must be TCP
    self.port = cleartext? ? 80 : 443 if port.blank?
  end

  # For these special subclasses, we are going to look up the service type by name. If the user has
  # a nonstandard configuration, this is going to result in the port in the Service instance not
  # matching up with the associated ApplicationType instance, but that's OK.
  def set_application_type
    app_type =
      if cleartext?
        ApplicationType.find_by(name: 'http', port: 80, transport_protocol: 'tcp')
      else
        ApplicationType.find_by(name: 'https', port: 443, transport_protocol: 'tcp')
      end
    self.application_type = app_type
  end

  # Helpers
  def web_application_config_options
    {
      web_service_protocol: ssl? ? 'https' : 'http',
      web_service_url_blacklist: web_application_config&.url_blacklist,
      web_service_auth_type: web_application_config&.auth_type,
      web_service_starting_page_path: web_application_config&.starting_page_path,
      web_service_login_form_username_field_name: web_application_config&.login_form_username_field_name,
      web_service_login_form_password_field_name: web_application_config&.login_form_password_field_name
    }
  end
end
