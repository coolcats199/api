# frozen_string_literal: true

module Perishable
  extend ActiveSupport::Concern

  included do
    scope(:expired, ->(age = 24.hours) { where('updated_at <= ?', age.ago) })
  end
end
