# frozen_string_literal: true

module UncachedQuery
  def key_pair_assignment_exists_for_machine?(machine)
    SshKeyPairAssignment.exists?(machine: machine)
  end

  def ssh_services_exists_for_machine?(machine)
    SshService.where(machine: machine).exists?
  end

  def org_configuration_uses_relay_ssh_key?(org)
    OrganizationConfiguration.where(organization: org, use_relay_ssh_key: true).exists?
  end
end
