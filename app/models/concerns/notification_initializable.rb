# frozen_string_literal: true

module NotificationInitializable
  extend ActiveSupport::Concern

  included do
    before_create :build_default_notification_channels

    private

    # populate default notifications from config/notifications.yml
    def build_default_notification_channels
      NOTIFICATIONS.each_key { |k| notification_channels.build(event: k) }
    end
  end
end
