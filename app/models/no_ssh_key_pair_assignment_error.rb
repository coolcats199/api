# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

class NoSshKeyPairAssignmentError < NoSshKeyPairError
  MESSAGE = 'You have one or more tests enabled that require authentication, but no SSH Key Pairs have been assigned.'

  class << self
    def check(org, _options = {})
      required_ssh_key_pair_missing?(org) ? create_error(org) : remove_error(org)
    end

    def message
      MESSAGE
    end

    private

    def required_ssh_key_pair_missing?(org)
      org_has_machines?(org) &&
        !relay_using_ssh_key_pair_or_ssh_key_pair_assignment?(org) &&
        org_has_authenticated_tests_enabled?(org)
    end

    def relay_using_ssh_key_pair_or_ssh_key_pair_assignment?(org)
      # If the organization has a relay with 'use relay ssh key' configuration
      # selected, the machine is not required to have an ssh key pair assigned to
      # it because the ssh key pair may exist on the relay box. If these conditions
      # do not hold, an ssh key pair assignment must be present.
      org_has_relay_using_ssh_key?(org) || org_machines_have_ssh_key_pair_assignments?(org)
    end

    def org_machines_have_ssh_key_pair_assignments?(org)
      SshKeyPairAssignment.where(machine_id: org.machines).exists?
    end

    def org_has_machines?(org)
      # If an organization has no machines then no ssh key assignment can be made.
      org.machines.exists?
    end
  end

  def message
    MESSAGE
  end
end
