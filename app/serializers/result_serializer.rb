# frozen_string_literal: true

# == Schema Information
#
# Table name: results
#
#  id                    :integer          not null, primary key
#  status                :integer          default("fail"), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  sir                   :integer          default("unevaluated"), not null
#  assessment_id         :integer
#  ignored               :boolean          default(FALSE), not null
#  signature             :string           not null
#  reported              :boolean          default(FALSE), not null
#  title_encrypted       :string
#  description_encrypted :string
#  output_encrypted      :string
#  nid_encrypted         :string
#
# Indexes
#
#  index_results_on_assessment_id  (assessment_id)
#  index_results_on_signature      (signature)
#
# Foreign Keys
#
#  fk_rails_1b008bfc36  (assessment_id => assessments.id) ON DELETE => cascade
#

class ResultSerializer < ActiveModel::Serializer
  attributes :id, :nid, :sir, :output, :status, :title, :description, :ignored,
             :signature, :reported, :updated_at, :created_at, :assessment_id
end
