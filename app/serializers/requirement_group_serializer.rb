# frozen_string_literal: true

# == Schema Information
#
# Table name: requirement_groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class RequirementGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :requirements
end
