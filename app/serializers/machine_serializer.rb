# frozen_string_literal: true

# == Schema Information
#
# Table name: machines
#
#  id                 :integer          not null, primary key
#  organization_id    :integer
#  ip                 :inet
#  fqdn               :string
#  description        :text
#  machine_status     :integer          default("pending"), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :string           not null
#  use_fqdn_as_target :boolean          default(FALSE), not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#

class MachineSerializer < ActiveModel::Serializer
  attributes :id,
             :ip,
             :fqdn,
             :description,
             :name,
             :latest_assessment_stats,
             :scheduled_service_discovery,
             :organization_id,
             :use_fqdn_as_target,
             :target_address

  has_one :ssh_key_pair
  has_many :services

  def scheduled_service_discovery
    object.service_discoveries.scheduled
  end

  def ip
    object.ip_to_s
  end
end
