# frozen_string_literal: true

class DockerCommandMachineSummarySerializer < ActiveModel::Serializer
  attributes :machines, :latest_assessment_created_at

  def machines
    object.query.results_summary
  end

  def latest_assessment_created_at
    object.query.latest_assessment_created_at
  end
end
