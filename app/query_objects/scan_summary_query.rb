# frozen_string_literal: true

class ScanSummaryQuery < SummaryQuery
  def assessments_summary
    # Iterate through the array of assessments and aggregate all necessary details into the summary hash
    (reduced_by_assessment_created_ats + reduced_by_assessment_states).each do |assessment|
      build_summary_item(assessment)
    end

    summary_hash
  end

  private

  def summary_hash
    # Initialize the summary hash with defaults for all the docker commands relevant to this
    # summary. Assessment stats will later replace created_at and state for docker commands
    # that have child assessments.
    @summary ||= relevant_docker_commands.each_with_object({}) do |dc, obj|
      initialize_summary_item(dc, obj)
    end.with_indifferent_access
  end

  def relevant_docker_commands
    docker_commands.order('docker_commands.created_at DESC').limit(self.class::SCAN_HISTORY_LIMIT)
  end

  def assessments_scope_hash
    { docker_command: relevant_docker_commands }
  end

  def initialize_summary_item(dc, obj)
    obj[dc.id] = {}
    obj[dc.id][:created_at] = dc.created_at
    obj[dc.id][:state] = dc.state
    obj[dc.id][:state_details] = DockerCommandDecorator.new(dc).state_details
  end

  def build_summary_item(assessment)
    dc_id = assessment.docker_command_id
    assign_assessment_stats_to_summary_item(summary_hash[dc_id], assessment)
  end

  def assign_assessment_stats_to_summary_item(summary_item, assessment)
    summary_item[:created_at] = assessment.created_at if assessment.has_attribute?(:created_at)
    summary_item[:state] = assessment.state if assessment.has_attribute?(:state)
    summary_item[:machines_scanned] = memoized_machines_scanned[assessment.docker_command_id]
    summary_item[:results_summary] = memoized_result_counts[assessment.docker_command_id]
  end

  def memoized_machines_scanned
    @machines_scanned ||= reduced_by_machines_scanned
  end

  def transform_result_counts_by_dc_ids(counts)
    @transformed_counts ||= counts.each_with_object({}) do |((dc_id, status), count), obj|
      obj[dc_id] ||= DEFAULT_STATUSES.dup
      obj[dc_id][Result::HUMAN_STATUSES[status]] = count
    end.with_indifferent_access
  end

  def memoized_result_counts
    @result_counts ||= transform_result_counts_by_dc_ids(reduced_by_result_counts)
  end

  def reduced_by_machines_scanned
    Assessment
      .where(assessments_scope_hash)
      .group(:docker_command_id)
      .distinct
      .count(:machine_id)
      .with_indifferent_access
  end

  # Returns states for assessments based on the least progress made.
  def reduced_by_assessment_states
    Assessment
      .where(assessments_scope_hash) # load all docker_commands for this org
      .order(docker_command_id: :desc, state: :asc) # order by the least-completed progress state
      .select('DISTINCT ON (docker_command_id) docker_command_id, state, id') # select first row of each group
  end

  # Returns created_ats for assessments based on the most recently created.
  def reduced_by_assessment_created_ats
    Assessment
      .where(assessments_scope_hash)
      .order(docker_command_id: :desc, created_at: :desc)
      .select('DISTINCT ON (docker_command_id) docker_command_id, created_at, id')
  end

  def primary_grouping_clause
    'assessments.docker_command_id'
  end
end
