# frozen_string_literal: true

module V1
  class ResultsController < ApplicationController
    wrap_parameters false
    before_action :set_assessment_object, only: :create
    before_action :require_container_secret_signature, only: :create
    before_action :add_namespace_to_vuln_id, only: :create
    skip_before_action :require_api_token, only: :create

    def create
      @assessment.create_result_set!(creation_params)
      render json: :ok
    rescue ActionController::ParameterMissing => e
      @assessment.results.create_parameter_error_result(@assessment, e)
      raise e
    rescue => e
      @assessment.results.create_unexpected_error_result(@assessment, e)
      raise e
    end

    private

    def creation_params
      params.require(:results).map do |p|
        p.permit(:nid, :status, :sir, :output, :title, :description, :signature)
      end
    end

    def add_namespace_to_vuln_id
      return unless params[:results]
      params[:results].each do |result|
        result[:nid] = "#{@assessment.title.match(%r{\A.+\/(.+):.+\z}).captures[0]}\:#{result[:nid]}"
      end
    end

    def set_assessment_object
      @assessment = Assessment.find params[:assessment_id]
    end

    def container_secret
      @assessment.security_container_secret&.secret
    end
  end
end
