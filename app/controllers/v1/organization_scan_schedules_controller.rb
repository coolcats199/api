# frozen_string_literal: true

module V1
  class OrganizationScanSchedulesController < V1::ApplicationController
    before_action :set_schedule, only: %i[show update destroy]
    before_action :set_org, only: %i[index create]

    def index
      render json: @org.scan_schedules
    end

    def create
      schedule = @org.scan_schedules.build(schedule_params)
      if schedule.save
        render json: schedule
      else
        render_errors_for(schedule)
      end
    end

    def show
      render json: @schedule
    end

    def update
      if @schedule.update(schedule_params)
        render json: @schedule
      else
        render_errors_for(@schedule)
      end
    end

    def destroy
      @schedule.destroy
      head :no_content
    end

    private

    def set_schedule
      @schedule = OrganizationScanSchedule.find(params[:id])
      authorize_action_for @schedule
    end

    def set_org
      @org = Organization.find(params[:organization_id])
      authorize_action_for OrganizationScanSchedule, in: @org
    end

    def schedule_params
      params.require(:organization_scan_schedule).permit(:period, :at)
    end
  end
end
