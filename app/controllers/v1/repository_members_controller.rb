# frozen_string_literal: true

module V1
  class RepositoryMembersController < ApplicationController
    before_action :set_repository, only: %i[index create]
    before_action :set_member, only: :destroy

    # GET /security_test_repositories/repository_members
    def index
      render json: @repository.repository_members
    end

    # POST /security_test_repositories/repository_members
    def create
      member = @repository.repository_members.build(creation_params)

      if member.save
        render json: member
      else
        render_errors_for member
      end
    end

    # DELETE /repository_members/:id
    def destroy
      @member.destroy!
      head :no_content
    end

    private

    def set_repository
      @repository = SecurityTestRepository.find(params[:security_test_repository_id])
      authorize_action_for RepositoryMember, in: @repository
    end

    def set_member
      @member = RepositoryMember.find(params[:id])
      authorize_action_for @member
    end

    def creation_params
      member_attrs = member_params

      # Replace uid with the actual user
      member_attrs.merge user: User.find_by!(uid: member_attrs.delete(:uid))
    end

    def member_params
      params.require(:repository_member).permit(:uid, :role_type)
    end
  end
end
