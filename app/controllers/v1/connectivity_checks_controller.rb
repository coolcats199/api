# frozen_string_literal: true

require './lib/norad_relay_helpers'

module V1
  class ConnectivityChecksController < ApplicationController
    include NoradRelayHelpers

    # This prevents the update action from failing with a 401: Unauthorized. This happens because Rails injects
    # the param wrapper which is included in the HMAC computation on the API side, but not present in the HMAC
    # computation on the Beacon side.
    wrap_parameters false

    before_action :set_machine, except: :update
    before_action :set_check, only: :update
    before_action :require_container_secret_signature, only: :update
    skip_before_action :require_api_token, only: :update

    # POST /v1/machines/:machine_id/connectivity_checks
    def create
      authorize_action_for MachineConnectivityCheck, in: @machine.organization
      check = @machine.send(controller_name).build
      check.build_security_container_secret
      if check.save
        ScheduleMachineConnectivityCheckJob
          .perform_later(check.id, relay_opts(@machine.organization))
        render json: check
      else
        render_errors_for(check)
      end
    end

    # GET /v1/machines/:machine_id/connectivity_checks
    def index
      authorize_action_for MachineConnectivityCheck, in: @machine.organization
      render json: @machine.send(controller_name)
    end

    # This action is only called from a container (i.e. within a Relay) and authorization
    # is not relevant because the check's secret is already verified.
    # PATCH /v1/[ping_connectivity_checks|ssh_connectivity_checks]/:id
    def update
      return render(json: @check) if @check.update(update_params)
      render_errors_for(@check)
    end

    # GET /v1/machines/:machine_id/connectivity_checks/:id
    def show
      check = find_or_latest
      authorize_action_for check
      render json: check
    end

    private

    def update_params
      params.require(:connectivity_check).permit(:status, :status_reason).merge(finished_at: Time.now.utc)
    end

    def find_or_latest
      relation = @machine.send(controller_name)
      params[:id] == 'latest' ? relation.latest! : relation.find(params[:id])
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
    end

    def set_check
      @check = controller_name.classify.constantize.find(params[:id])
    end

    def container_secret
      @check.shared_secret
    end
  end
end
