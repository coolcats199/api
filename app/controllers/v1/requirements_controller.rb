# frozen_string_literal: true

module V1
  class RequirementsController < ApplicationController
    before_action :set_group, only: [:create]
    before_action :set_requirement, only: %i[show destroy update]

    def create
      authorize_action_for Requirement, in: @group
      requirement = @group.requirements.build(requirement_params)
      if requirement.save
        render json: requirement
      else
        render json: { errors: requirement.errors }, status: :unprocessable_entity
      end
    end

    def update
      if @requirement.update(requirement_params)
        render json: @requirement
      else
        render json: { errors: @requirement.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      @requirement.destroy!
      head :no_content
    end

    def show
      render json: @requirement
    end

    private

    def set_requirement
      @requirement = Requirement.find(params[:id])
      authorize_action_for @requirement
    end

    def set_group
      @group = RequirementGroup.find(params[:requirement_group_id])
    end

    def requirement_params
      params.require(:requirement).permit(:name, :description)
    end
  end
end
