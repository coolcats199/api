# frozen_string_literal: true

# Other authorizers should subclass this one
class ApplicationAuthorizer < Authority::Authorizer
  class << self
    # Any class method from Authority::Authorizer that isn't overridden
    # will call its authorizer's default method.
    #
    # @param [Symbol] adjective; example: `:creatable`
    # @param [Object] user - whatever represents the current user in your app
    # @return [Boolean]
    def default(_adjective, _user)
      # 'Whitelist' strategy for security: anything not explicitly allowed is
      # considered forbidden.
      false
    end

    private

    def admin?(user, org)
      user.has_role? :organization_admin, org
    end

    def reader?(user, org)
      user.has_role? :organization_reader, org
    end
  end

  def default(_adjective, _user)
    false
  end

  private

  def admin?(user, org)
    user.has_role? :organization_admin, org
  end

  def reader?(user, org)
    user.has_role? :organization_reader, org
  end
end
