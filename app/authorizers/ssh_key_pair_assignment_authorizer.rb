# frozen_string_literal: true

class SshKeyPairAssignmentAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def deletable_by?(user)
    admin?(user, resource.machine.organization)
  end
end
