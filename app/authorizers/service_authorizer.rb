# frozen_string_literal: true

class ServiceAuthorizer < ApplicationAuthorizer
  def self.readable_by?(user, options)
    reader?(user, options[:in]) || admin?(user, options[:in])
  end

  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def readable_by?(user)
    reader?(user, org) || admin?(user, org)
  end

  def updatable_by?(user)
    admin?(user, org)
  end

  def deletable_by?(user)
    admin?(user, org)
  end

  private

  def org
    resource.organization
  end
end
