# frozen_string_literal: true

class AssessmentAuthorizer < ApplicationAuthorizer
  def self.readable_by?(user, options)
    reader?(user, options[:in]) || admin?(user, options[:in])
  end

  def readable_by?(user)
    org = resource.machine.organization
    reader?(user, org) || admin?(user, org)
  end
end
