# frozen_string_literal: true

class ResultExporterAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in]) || reader?(user, options[:in])
  end
end
