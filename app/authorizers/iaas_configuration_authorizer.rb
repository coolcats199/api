# frozen_string_literal: true

class IaasConfigurationAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user, options)
    admin?(user, options[:in])
  end

  def deletable_by?(user)
    admin?(user, resource.organization)
  end

  def readable_by?(user)
    admin?(user, resource.organization)
  end

  def updatable_by?(user)
    admin?(user, resource.organization)
  end
end
