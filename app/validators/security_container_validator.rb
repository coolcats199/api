# frozen_string_literal: true

module SecurityContainerValidator
  extend ActiveSupport::Concern

  TARGET_PARAM = '%{target}'
  SSH_USER_PARAM = '%{ssh_user}'
  SSH_KEY_PARAM = '%{ssh_key}'
  TARGET_SHIM = { target: 'validation placeholder' }.freeze
  SSH_SHIM = { ssh_key: 'abcdef', ssh_user: 'user' }.freeze

  included do
    validate :program_args_specifies_target_parameter
    validate :white_box_containers_specify_ssh_parameters, if: :whitebox?
    validate :test_types_are_valid
    validate :whole_host_is_mutually_exclusive
    validate :test_types_is_not_empty
    validate :default_config_is_a_hash

    private

    def program_args_specifies_target_parameter
      if prog_args && prog_args.scan(TARGET_PARAM).length.zero?
        errors.add(:prog_args, 'must specify target parameter')
      end
      true
    end

    def white_box_containers_specify_ssh_parameters
      if prog_args && prog_args.scan(SSH_USER_PARAM).length != 1 && prog_args.scan(SSH_KEY_PARAM).length != 1
        errors.add(:prog_args, 'must specify ssh parameters')
      end
      true
    end

    def prog_args_contains_parameters?
      return false unless prog_args
      # Another validation ensures that the target parameter is present
      prog_args.scan(/%{\w+}/).length > 1
    end

    def test_types_are_valid
      return unless test_types&.any? { |t| !SecurityContainer::TEST_TYPES.include?(t) }
      errors.add(:test_types, "must only contain the following: #{SecurityContainer::TEST_TYPES.join(', ')}")
    end

    def whole_host_is_mutually_exclusive
      return unless test_types&.include?('whole_host') && test_types.length > 1
      errors.add(:test_types, 'can only contain the whole_host option if it is specified')
    end

    def test_types_is_not_empty
      errors.add(:test_types, 'must specify at least one type') if test_types&.empty?
    end

    def default_config_is_a_hash
      errors.add(:default_config, 'must be a Hash') unless default_config.is_a?(Hash)
    end
  end
end
