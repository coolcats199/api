# frozen_string_literal: true

require './lib/norad_exception_notifier'

class ScheduleScanJob
  include NoradExceptionNotifier

  @queue = :scan_scheduling

  class << self
    include ContainerQueryHelpers

    def perform(gid)
      @schedule = GlobalID::Locator.locate gid
      @schedule.scan_target.docker_commands.create!(scan_containers_attributes: enabled_containers)
    rescue StandardError => exception
      # Airbrake doesn't wrap errors for plain Ruby classes, so catch the
      # notification here, notify Airbrake manually and re-raise the exception to Resque.
      # For more investigation, see where the Airbrake wrappers are loaded here:
      # https://github.com/airbrake/airbrake/tree/master/lib/airbrake/rails
      notify_airbrake(exception)
      raise exception
    end

    private

    def organization
      @schedule.organization
    end

    def machine_ids
      organization.machines.select(:id)
    end
  end
end
