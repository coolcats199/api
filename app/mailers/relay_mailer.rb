# frozen_string_literal: true

class RelayMailer < ApplicationMailer
  def offline_notification(relay)
    @relay = relay
    mail to: admin_emails, subject: "#{SUBJECT_PREFIX} Your Norad Relay is now Offline"
  end

  def online_notification(relay)
    @relay = relay
    mail to: admin_emails, subject: "#{SUBJECT_PREFIX} Your Norad Relay is now Online"
  end

  private

  def admin_emails
    @relay.organization.admins.map(&:email)
  end
end
