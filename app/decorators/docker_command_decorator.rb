# frozen_string_literal: true

class DockerCommandDecorator < SimpleDelegator
  NO_ASSESSMENTS = 'No relevant assessments'
  FINISHED_SUCCESS = 'Finished successfully'
  STARTED_SUCCESS = 'Started successfully'

  def state_details
    return error_details_message if error_details.present?
    return no_assessments_message if no_assessments?
    return finished_success_message if complete?
    started_success_message if assessments_created?
  end

  private

  def no_assessments?
    complete? && assessments.count.zero?
  end

  def complete?
    assessments_created? && finished_at.present?
  end

  def error_details_message
    "Scan failed: #{error_details}"
  end

  def no_assessments_message
    NO_ASSESSMENTS
  end

  def finished_success_message
    FINISHED_SUCCESS
  end

  def started_success_message
    STARTED_SUCCESS
  end
end
