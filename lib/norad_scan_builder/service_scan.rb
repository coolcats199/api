# frozen_string_literal: true

module NoradScanBuilder
  # Scan a host's services with different arguments per service
  class ServiceScan < NoradScan
    def post_initialize(opts)
      machine = opts.fetch(:machine)
      service = opts.fetch(:service)
      @args = ServiceScanArgs.new(container, service: service)
      @target = ServiceScanTarget.new(container, secret_id, dc, machine, service: service) unless @args.to_a.empty?
    end
  end
end
