# frozen_string_literal: true

module NoradScanBuilder
  # Scan multiple machines with the same set of arguments
  # XXX Currently multi host scanning is only supported for whole host scans like Qualys
  class MultiHostScan < NoradScan
    def post_initialize(opts)
      org = opts.fetch(:organization)
      machines = opts.fetch(:machines)
      @args = MultiHostScanArgs.new(container, organization: org, machines: machines)
      return if @args.to_a.empty?
      @target = machines.map do |machine|
        MultiHostScanTarget.new(container, secret_id, dc, machine, organization: org)
      end
    end
  end
end
