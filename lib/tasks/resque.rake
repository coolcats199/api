# frozen_string_literal: true

unless Rails.env.test?
  require 'resque/tasks'

  namespace :resque do
    task 'resque:setup' => :environment do
      require 'resque'
    end
  end
end

if Rails.env.production?
  require 'resque/tasks'
  require 'resque/scheduler/tasks'

  namespace :resque do
    task setup_schedule: :setup do
      require 'resque-scheduler'
      Resque::Scheduler.dynamic = true
      Resque::Scheduler.configure do |c|
        c.quiet = false
        c.verbose = true
        c.logformat = 'json'
      end
      Resque.schedule = YAML.load_file(Rails.root.join('config/schedule.yml'))
    end

    task scheduler: :setup_schedule
  end
end
