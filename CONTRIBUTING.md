# How to contribute

This guide is meant to provide guidelines for how to contribute to the Norad API project. These
procedures should work in the majority of cases. At times, it will be necessary to step outside of
these guidelines and Do What Makes Sense. This is an living document that we can continue to modify as
we define the process that helps us add code quickly and safely.

## Getting Started

* If you haven't already, you'll want to set up a [development box](https://gitlab.com/norad/dev-box).
  This provides a consistent environment for running tests.
* We use [rubocop](https://github.com/bbatsov/rubocop) to enforce code quality and style guidelines.
  You can familiarize yourself with the [style guide](https://github.com/bbatsov/ruby-style-guide).
  You can also install a tool into your editor which will help you follow the style guideline. One
  for vim can be found [here](https://github.com/scrooloose/syntastic).

## Making Changes

Fundamentally, we follow the Github Flow(1) methodology for branching and committing code. The
process essentially breaks down as follows:

1. Assign the feature you are beginning to work on to yourself in JIRA.
2. Create a feature branch off of the master branch. Note that anything in master is always
   deployable, so this branch will always have the latest code. Branch names should be descriptive.
3. Your branch is your branch. Feel free to experiment and commit freely. You may wish to make your
   life easier when it's time to merge into master by learning about [auto-squashing
   commits](https://robots.thoughtbot.com/autosquashing-git-commits).[^1]
4. You are free to push the feature branch to the origin as freqently as you wish. This will run the
   CI suite. You can also run the test suite locally on your dev box. By pushing to the feature
   branch, you can engage other engineers on the team if you need help. They can checkout your
   branch and examine the code.
5. When your feature is done, open a Merge Request in Gitlab.

At this point in process, there is no requirement of any kind for your commit messages. Before
merging into master, though, you will need a "good commit message" for any commits that aren't being
squashed. Links to further reading on this topic are available in the Additional Resources section.

## Submitting Changes

When your feature is ready, or is at a state where you'd like to formally begin engaging the rest of
the team, create a [Merge Request](https://gitlab.com/norad/api/merge_requests) in
Gitlab. Please write a meaningful title and detailed description that provides an overview of What
your feature does and Why it does it. You can leave it to the code itself to explain the How, but
feel free to call anything out here that you would like the reviewer(s) to pay special attention to.
If you feel that someone in particular needs to look at your Merge Request, you may assign the MR to
them. Mentioning someone in the Merge Request description is another way to alert someone in
particular you'd like for them to look at your MR, e.g. ```cc @bmanifol```.

**ProTips**

* Mention the issue that this MR resolves in JIRA, and Gitlab will hotlink and it close the issue for
you when you merge. e.g. ```Closes ISEC_CSE-1234```.

* Gitlab will autofill the title and description with your most recent commit when opening the Merge
Request. So, writing a good commit for your last commit before opening the Merge Request can help
you kill two birds with one stone.

## Code Review Process

The Code Review Process is meant to be an open-ended discussion about the feature that is being
committed. It should address whether the changeset meets the requirements of the feature as well as
the quality of the code itself. The reviewer(s) should also make sure that the feature includes an
adequate number of meaningful tests.

Please be polite in both giving and receiving feedback. Remember that it's a review of the code, not
an individual. As a reviewer, do your best to separate your personal opinion of how you would do
something from what is objectively constructive criticism. Please review the Thoughtbot guide for
Code Reviews in the Additional Resources section (3).

Make any changes to your feature branch that you and the reviewer agree should be made, and push
those changes to your feature branch. This will automatically update your Merge Request. Repeat this
process until the Merge Request is approved.

The merge request must receive at least one **+1** before it can be merged. If individuals are
called out to look at something during any point of the review process, they will need to provide a
**+1** as well before it can be merged.

## Merge Process

Your feature got approved! Great! Now it's time to merge into master.

1. Before code can be merged to master, **all tests must be passing**. In other words, you need a
   green check from Gitlab CI.
2. In order to keep the master branch's commit history clean, you may be required to rebase your
   feature branch before it can be merged. This is a loose a requirement, but please consider
   squashing interim commits into one or more block commits. The goal here is to keep the master
   branch devoid of commits such as "fixing typo" or "trying out this method." To rebase, run the
   following on your freature branch: ```git rebase -i origin/master```. See the additional
   resources for more information on rebasing.
3. After (force) pushing your rebased feature branch, the Merge Request can be merged **as long as
   all tests still pass**.

# Additional Resources

1. [Github Flow](https://guides.github.com/introduction/flow/)
2. [Code Review Guide](https://github.com/thoughtbot/guides/tree/master/code-review)
3. [Style Guide](https://github.com/bbatsov/ruby-style-guide)
4. [Git Tips](https://github.com/thoughtbot/guides/blob/master/protocol/git)
5. [Git Rebase](https://help.github.com/articles/about-git-rebase/)
6. [Auto-squashing Git Commits](https://robots.thoughtbot.com/autosquashing-git-commits)
7. [Good Commit Messages](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
8. [More Good Commit Messages](http://chris.beams.io/posts/git-commit/)
9. [Even More Good Commit Messages](http://www.alexkras.com/19-git-tips-for-everyday-use/#good-commit-message)

[^1]: Some background and additional reading on squashing commits. [Squashing Commits with Git](https://davidwalsh.name/squash-commits-git) deals with squashing commits in a feature branch. [git rebase --autosquash](https://coderwall.com/p/hh-4ea/git-rebase-autosquash) picks up where the previous article leaves off and explains how the "git rebase" option "--autosquash" complements squashing commits.
