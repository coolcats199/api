# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
# require "sprockets/railtie"
require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# Require our custom exceptions
require_relative '../lib/norad_api_exceptions'
require_relative '../lib/norad_exception_notifier'

module NoradApi
  class Application < Rails::Application
    include NoradExceptionNotifier
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true
    config.middleware.use Rack::Attack
    config.debug_exception_response_format = :api

    config.exceptions_app = lambda do |env|
      if Rails.env.production?
        exception = env['action_dispatch.exception']
        status = env['PATH_INFO'][1..-1]
        notify_airbrake(exception) if %w[429 500].include? status
      end
      ::ActionDispatch::PublicExceptions.new(Rails.public_path).call(env)
    end
  end
end
