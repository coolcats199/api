class CreateRepositoryWhitelistEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :repository_whitelist_entries do |t|
      t.references :organization, null: false, index: false, foreign_key: { on_delete: :cascade }
      t.references :security_test_repository, null: false, index: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
    add_index :repository_whitelist_entries,
              [:organization_id, :security_test_repository_id],
              name: :index_r_w_entries_on_o_id_and_s_t_r_id,
              unique: true
  end
end
