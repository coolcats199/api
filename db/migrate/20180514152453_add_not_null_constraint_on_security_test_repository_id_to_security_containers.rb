class AddNotNullConstraintOnSecurityTestRepositoryIdToSecurityContainers < ActiveRecord::Migration[5.0]
  def change
    change_column_null :security_containers, :security_test_repository_id, false
  end
end
