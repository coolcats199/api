class AddStatusReasonToMachineConnectivityChecks < ActiveRecord::Migration[5.0]
  FAILING_STATUS=1

  def up
    add_column :machine_connectivity_checks, :status_reason, :text
    populate_default_reason_for_failed_checks
  end

  def down
    remove_column :machine_connectivity_checks, :status_reason
  end

  private

  def populate_default_reason_for_failed_checks
    execute ssh_failed_update_sql
    execute ping_failed_update_sql
  end

  def ssh_failed_update_sql
    <<~SQL
      UPDATE machine_connectivity_checks
      SET status_reason='There was an issue pinging this machine.'
      WHERE type='PingConnectivityCheck'
            AND finished_at IS NOT NULL
            AND status = #{FAILING_STATUS}
    SQL
  end

  def ping_failed_update_sql
    <<~SQL
      UPDATE machine_connectivity_checks
      SET status_reason='There was an issue trying to ssh to this machine.'
      WHERE type='SshConnectivityCheck'
            AND finished_at IS NOT NULL
            AND status = #{FAILING_STATUS}
    SQL
  end
end
