# frozen_string_literal: true
class EnableVaultForDockerRelay < ActiveRecord::Migration[4.2]
  def up
    add_column :docker_relays, :file_encryption_key_encrypted, :string
    DockerRelay.reset_column_information

    DockerRelay.all.each do |relay|
      relay.file_encryption_key = relay.attributes['file_encryption_key']
      relay.save!
    end

    remove_column :docker_relays, :file_encryption_key
  end

  def down
    remove_column :docker_relays, :file_encryption_key_encrypted
    add_column :docker_relays, :file_encryption_key, :string
  end
end
