class CreateMachineConnectivityChecks < ActiveRecord::Migration[5.0]
  def change
    create_table :machine_connectivity_checks do |t|
      t.references :machine, foreign_key: { on_delete: :cascade }, index: false, null: false
      t.references :security_container_secret, foreign_key: { on_delete: :cascade }, index: false, null: false
      t.string :organization_error_type, null: false
      t.datetime :finished_at
      t.index %i[machine_id organization_error_type],
              name: :index_machine_connectivity_checks_on_m_id_and_o_e_type
      t.index :security_container_secret_id,
              name: :index_machine_connectivity_checks_on_s_c_s_id,
              unique: true

      t.timestamps
    end
  end
end
