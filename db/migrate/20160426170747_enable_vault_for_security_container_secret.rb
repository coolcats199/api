# frozen_string_literal: true
class EnableVaultForSecurityContainerSecret < ActiveRecord::Migration[4.2]
  def up
    add_column :security_container_secrets, :secret_encrypted, :string
    SecurityContainerSecret.reset_column_information

    SecurityContainerSecret.all.each do |container|
      container.secret = container.attributes['secret']
      container.save!
    end

    remove_column :security_container_secrets, :secret
  end

  def down
    remove_column :security_container_secrets, :secret_encrypted
    add_column :security_container_secrets, :secret, :string
  end
end
