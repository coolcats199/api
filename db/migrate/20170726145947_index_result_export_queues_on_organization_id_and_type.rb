class IndexResultExportQueuesOnOrganizationIdAndType < ActiveRecord::Migration[5.0]
  def change
    add_index :result_export_queues, %i[organization_id type], unique: true, where: "type = 'InfosecExportQueue'"
    change_column_null :result_export_queues, :organization_id, false
  end
end
