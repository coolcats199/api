class RemovePolymorphicCommandableColumnFromDockerCommands < ActiveRecord::Migration[4.2]
  class DockerCommand < ActiveRecord::Base
    belongs_to :commandable, polymorphic: true
    belongs_to :machine
    belongs_to :organization
  end

  def up
    change_table :docker_commands do |t|
      t.references :machine, index: true
      t.references :organization, index: true
    end
    add_foreign_key :docker_commands, :machines, on_delete: :cascade
    add_foreign_key :docker_commands, :organizations, on_delete: :cascade
    DockerCommand.reset_column_information

    DockerCommand.all.each do |dc|
      if dc.commandable_type == 'Organization'
        dc.organization_id = dc.commandable_id
      else
        dc.machine_id = dc.commandable_id
      end
      dc.save!
    end

    change_table :docker_commands do |t|
      t.remove :commandable_type
      t.remove :commandable_id
    end
  end

  def down
    change_table :docker_commands do |t|
      t.references :commandable, polymorphic: true, index: true
    end
    DockerCommand.reset_column_information

    DockerCommand.all.each do |dc|
      if dc.machine
        dc.commandable = dc.machine
      elsif dc.organization
        dc.commandable = dc.organization
      end
      dc.save!
    end

    change_table :docker_commands do |t|
      t.remove :machine_id
      t.remove :organization_id
    end
  end
end
