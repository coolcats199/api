class ConvertMachinesIpToInet < ActiveRecord::Migration[5.0]
  def up
    Machine.where(ip: '').update_all(ip: nil)
    change_column :machines, :ip, 'inet USING ip::inet'
  end

  def down
    change_column :machines, :ip, :string
  end
end
