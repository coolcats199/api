class DropUselessSecurityContainers < ActiveRecord::Migration[4.2]
  def up
    SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/serverspec:0.0.1')&.delete
    SecurityContainer.find_by(name: 'norad-registry.cisco.com:5000/hydra-web-form:0.0.1')&.delete
  end

  def down
    SecurityContainer.where(name: 'norad-registry.cisco.com:5000/serverspec:0.0.1').first_or_create! do |sc|
      sc.prog_args = '-h %{target} -u %{ssh_user} -t **/*_spec.rb -p %{port} %{ssh_key}'
      sc.category = :whitebox
      sc.test_types << 'authenticated'
    end
    SecurityContainer.where(name: 'norad-registry.cisco.com:5000/hydra-web-form:0.0.1').first_or_create! do |sc|
      sc.prog_args = '-s %{port} %{target} http-post-form %{path}:%{id_field}=^USER^&%{password_field}=^PASS'\
        '^%{extra_params}:S=%{success_string}'
      sc.default_config = {
        port: 80,
        path: '/dvwa/login.php',
        id_field: 'username',
        password_field: 'password',
        success_string: 'index.php',
        extra_params: '&Login=Login'
      }
      sc.category = :blackbox
      sc.test_types << 'brute_force' << 'web_application'
    end
  end
end
