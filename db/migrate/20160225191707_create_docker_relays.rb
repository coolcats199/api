class CreateDockerRelays < ActiveRecord::Migration[4.2]
  def change
    create_table :docker_relays do |t|
      t.references :organization, index: true, null: false
      t.text :public_key, null: false
      t.string :queue_name, null: false
      t.integer :state, null: false, default: 0
      t.datetime :last_heartbeat, null: false
      t.boolean :verified, null: false, default: false

      t.timestamps null: false
    end
    add_index :docker_relays, :queue_name, unique: true
    add_foreign_key :docker_relays, :organizations, on_delete: :cascade
  end
end
