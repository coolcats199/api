class UpdateQualysWasSecurityContainer < ActiveRecord::Migration[4.2]
  def up
    say_with_time 'Migrating Qualys-WAS Default Config' do
      sc = SecurityContainer.find_by name: 'norad-registry.cisco.com:5000/qualys-was:0.0.1'
      return unless sc
      say_with_time('Found the container.sc Updating the default_config value') do
        sc.default_config = { target_url: 'http://sample_url.com',
                              qualys_username: 'placeholder_qualys_username',
                              qualys_password: 'placeholder_qualys_password',
                              web_username: 'placeholder_web_username',
                              web_password: 'placeholder_web_password',
                              option_profile: 'NoradProfile',
                              qualys_scanner: 'SJC23-IPv6-2',
                              coma_sep_blacklist_urls: 'placeholder_coma_sep_blacklist_urls' }
        sc.save!
      end
    end
    say_with_time 'Update the SecurityContainter Config objects with scanner values other than regex(SJC23-IPv6-[12])' do
      was_container = SecurityContainer.find_by_name('norad-registry.cisco.com:5000/qualys-was:0.0.1')
      return unless was_container
      sc_configs = SecurityContainerConfig.where(security_container_id: was_container.id)
      if sc_configs && !sc_configs.empty?
        sc_configs.each do |sc_config|
          if sc_config.values['qualys_scanner'] == 'RTP-3'
            old_values = sc_config.values
            sc_config.values = nil
            sc_config.values = old_values
            sc_config.values['qualys_scanner'] = 'SJC23-IPv6-' + rand(1..2).to_s
            sc_config.save!
          end
        end
      end
    end
  end

  def down
    # The migration up method is changing the scanner from RTP-3 to SJCxxx,
    # Once set to that, I don't see any reason why we should go back to RTP-3,
    # as RTP-3 is not supposed to be used for Qualys-WAS. So, down is a no-op.
  end
end
