class AddServicesReferenceToWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  def change
    add_reference :white_box_assessments, :service, index: true
    add_foreign_key :white_box_assessments, :services, on_delete: :cascade
  end
end
