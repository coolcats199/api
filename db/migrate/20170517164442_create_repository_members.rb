class CreateRepositoryMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :repository_members do |t|
      t.references :user, null: false, index: true
      t.references :security_test_repository, null: false, index: true
      t.timestamps
    end

    add_index :repository_members,
              [:user_id, :security_test_repository_id],
              unique: true,
              name: 'index_repository_members_on_u_id_and_s_t_r_id'
    add_foreign_key :repository_members, :users, on_delete: :cascade
    add_foreign_key :repository_members, :security_test_repositories, on_delete: :cascade
  end
end
