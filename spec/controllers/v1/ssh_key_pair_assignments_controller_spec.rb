# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::SshKeyPairAssignmentsController, type: :controller, with_docker_doubled: true do
  describe 'POST #create' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @kp = create :ssh_key_pair, organization: @machine1.organization
    end

    context 'as an organization admin' do
      it 'creates a new key pair association for a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@machine1.ssh_key_pair).to be(nil)
        create_params = { machine_id: @machine1.to_param, ssh_key_pair_id: @kp.to_param }
        norad_post :create, create_params
        expect(response.status).to eq(200)
        expect(@machine1.reload.ssh_key_pair).to eq(@kp)
      end

      it 'requries the keypair to be in the same organization as the machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@machine1.ssh_key_pair).to be(nil)
        create_params = { machine_id: @machine2.to_param, ssh_key_pair_id: @kp.to_param }
        expect { norad_post :create, create_params }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context 'as an organization reader' do
      it 'cannot create associate a keypair with a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@machine1.ssh_key_pair).to be(nil)
        create_params = { machine_id: @machine1.to_param, ssh_key_pair_id: @kp.to_param }
        norad_post :create, create_params
        expect(@machine1.reload.ssh_key_pair).to be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create associate a keypair with a machine' do
        @_current_user.add_role :organization_reader, @machine2.organization
        expect(@machine1.ssh_key_pair).to be(nil)
        create_params = { machine_id: @machine1.to_param, ssh_key_pair_id: @kp.to_param }
        norad_post :create, create_params
        expect(@machine1.reload.ssh_key_pair).to be(nil)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @kp = create :ssh_key_pair, organization: @machine1.organization
      @machine1.ssh_key_pair = @kp
    end

    context 'as an organization admin' do
      it 'disassocciates a keypair with a machine' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@machine1.ssh_key_pair).to_not be(nil)
        norad_delete :destroy, machine_id: @machine1.to_param
        expect(@machine1.reload.ssh_key_pair).to be(nil)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot disassoicate a keypair from a machine' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@machine1.ssh_key_pair).to_not be(nil)
        norad_delete :destroy, machine_id: @machine1.to_param
        expect(@machine1.reload.ssh_key_pair).to_not be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete a schedule for a machine' do
        @_current_user.add_role :organization_reader, @machine2.organization
        expect(@machine1.ssh_key_pair).to_not be(nil)
        norad_delete :destroy, machine_id: @machine1.to_param
        expect(@machine1.reload.ssh_key_pair).to_not be(nil)
        expect(response.status).to eq(403)
      end
    end
  end
end
