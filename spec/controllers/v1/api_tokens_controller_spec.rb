# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::ApiTokensController, type: :controller do
  describe 'PUT #update' do
    context 'as an API user,' do
      it 'recreates the API token' do
        old_token = @_current_user.api_token
        norad_put :update, user_id: @_current_user.uid
        expect(response.status).to eq(200)
        @_current_user.reload
        expect(@_current_user.api_token.value).not_to eq(old_token.value)
      end

      it 'should not recreate another user\'s token' do
        @user = create :user
        token_value = @user.api_token.value
        norad_put :update, user_id: @user.uid
        expect(response.status).to eq(403)
        @user.reload
        expect(@user.api_token.value).to eq(token_value)
      end
    end
  end
end
