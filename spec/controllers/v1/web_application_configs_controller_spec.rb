# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::WebApplicationConfigsController, type: :controller do
  describe 'GET #show' do
    let(:web_app_config) { create(:web_application_config) }

    before(:each) do
      @_current_user.add_role :organization_admin, web_app_config.web_application_service.organization
    end

    it 'calls the authorizer' do
      expect(subject).to receive(:authorize_action_for)
      norad_get :show, id: web_app_config.to_param
    end

    it 'responds with success' do
      norad_get :show, id: web_app_config.to_param
      expect(response).to have_http_status :success
      expect(response).to match_response_schema('web_application_config')
    end
  end

  describe 'POST #create' do
    before :each do
      @machine1 = create :machine
      @service = create :web_application_service, machine: @machine1
      @machine2 = create :machine
    end

    let(:config_params) { { web_application_config: attributes_for(:web_application_config) } }

    context 'as an organization admin' do
      it 'creates a new config for a web service' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@service.web_application_config).to be(nil)
        create_params = config_params.merge(service_id: @service.to_param)
        norad_post :create, create_params
        expect(@service.reload.web_application_config).to_not be(nil)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('web_application_config')
      end
    end

    context 'as an organization reader' do
      it 'cannot create a new config for a web service' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@service.web_application_config).to be(nil)
        create_params = config_params.merge(service_id: @service.to_param)
        norad_post :create, create_params
        expect(@service.reload.web_application_config).to be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create a new config for a web service' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect(@service.web_application_config).to be(nil)
        create_params = config_params.merge(service_id: @service.to_param)
        norad_post :create, create_params
        expect(@service.reload.web_application_config).to be(nil)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @machine1 = create :machine
      @machine2 = create :machine
      @service1 = create :web_application_service, machine: @machine1
      @service2 = create :web_application_service, machine: @machine2
      @config = create :web_application_config, web_application_service: @service1
      @new_path = "/#{SecureRandom.hex}"
    end

    let(:config_params) { { web_application_config: { starting_page_path: @new_path } } }

    context 'as an organization admin' do
      it 'updates a config for a web service' do
        @_current_user.add_role :organization_admin, @machine1.organization
        expect(@config.starting_page_path).to_not eq(@new_path)
        update_params = config_params.merge(id: @config.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@config.reload.starting_page_path).to eq(@new_path)
      end
    end

    context 'as an organization reader' do
      it 'cannot update config for a web service' do
        @_current_user.add_role :organization_reader, @machine1.organization
        expect(@config.starting_page_path).to_not eq(@new_path)
        update_params = config_params.merge(id: @config.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      it 'cannot config for a web service' do
        @_current_user.add_role :organization_admin, @machine2.organization
        expect(@config.starting_page_path).to_not eq(@new_path)
        update_params = config_params.merge(id: @config.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:web_app_config) { create(:web_application_config) }

    before(:each) do
      @_current_user.add_role :organization_admin, web_app_config.web_application_service.organization
    end

    it 'calls the authorizer' do
      expect(subject).to receive(:authorize_action_for)
      norad_delete :destroy, id: web_app_config.to_param
    end

    it 'responds with success' do
      expect { norad_delete :destroy, id: web_app_config.to_param }.to change(WebApplicationConfig, :count).by(-1)
      expect(response.status).to eq(204)
    end
  end
end
