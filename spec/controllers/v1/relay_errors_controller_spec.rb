# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::RelayErrorsController, type: :controller do
  let(:valid_error_klass) { 'RelayQueueError' }
  let(:invalid_error_klass) { 'RelayOfflineError' }
  let(:relay) { create :docker_relay }

  before(:each) do
    allow_any_instance_of(V1::ApplicationController).to receive(:signed_by_key?).and_return(true)
    request.headers['HTTP_NORAD_SIGNATURE'] = 'anything'
  end

  describe 'POST #create' do
    let(:post_params) do
      {
        timestamp: Time.now.to_i,
        docker_relay_id: relay.to_param
      }
    end

    context 'when type is RelayQueueError' do
      it 'responds with no content' do
        expect do
          norad_post :create, post_params.merge(relay_error: { type: valid_error_klass })
        end.to change(RelayQueueError, :count).by(1)
        expect(response).to have_http_status :no_content
      end
    end

    context 'when validation fails' do
      before(:each) do
        allow_any_instance_of(RelayQueueError).to receive(:save).and_return(false)
      end

      it 'renders errors' do
        norad_post :create, post_params.merge(relay_error: { type: valid_error_klass })
        expect(response).to have_http_status :unprocessable_entity
        expect(response_body).to include 'errors'
      end
    end

    context 'when error already exists' do
      it 'responds with no content' do
        expect do
          norad_post :create, post_params.merge(relay_error: { type: valid_error_klass })
        end.to change(RelayQueueError, :count).by(1)
        expect(response).to have_http_status :no_content

        expect do
          norad_post :create, post_params.merge(relay_error: { type: valid_error_klass })
        end.not_to change(RelayQueueError, :count)
        expect(response).to have_http_status :no_content
      end
    end

    context 'when type is not RelayQueueError' do
      it 'overwrites type to RelayQueueError' do
        expect do
          norad_post :create, post_params.merge(relay_error: { type: invalid_error_klass })
        end.to change(RelayQueueError, :count).by(1)
        expect(response).to have_http_status :no_content
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:delete_params) { { timestamp: Time.now.to_i, docker_relay_id: relay.to_param } }

    context 'when type is RelayQueueError' do
      it 'responds with no content' do
        create :relay_queue_error, errable: relay
        expect do
          norad_delete :destroy, delete_params.merge(error_klass: valid_error_klass)
        end.to change(RelayQueueError, :count).by(-1)
        expect(response).to have_http_status :no_content
      end
    end

    context 'when error does not exist' do
      it 'responds with no content' do
        expect do
          norad_delete :destroy, delete_params.merge(error_klass: valid_error_klass)
        end.not_to change(OrganizationError, :count)
        expect(response).to have_http_status :no_content
      end
    end

    context 'when type is not RelayQueueError' do
      it 'overwrites type to RelayQueueError' do
        create :relay_queue_error, errable: relay
        expect do
          norad_delete :destroy, delete_params.merge(error_klass: invalid_error_klass)
        end.to change(RelayQueueError, :count).by(-1)
        expect(response).to have_http_status :no_content
      end
    end
  end
end
