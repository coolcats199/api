# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::OrganizationRequirementsController, type: :controller do
  describe 'GET #index' do
    before :each do
      @org = create :organization
      req_group1 = create :requirement_group, name: 'PSB'
      req_group2 = create :requirement_group, name: 'SDL'
      @reqs1 = Array.new(3) do
        create :requirement, requirement_group: req_group1
      end
      Array.new(3) do
        create :requirement, requirement_group: req_group2
      end
      @org.enforcements << build(:enforcement, requirement_group: req_group1)
    end

    context 'as an organization admin' do
      it 'returns the requirements enforced for an organization' do
        @_current_user.add_role :organization_admin, @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_requirements')
      end
    end

    context 'as an organization reader' do
      it 'returns the requirements enforced for an organization' do
        @_current_user.add_role :organization_reader, @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_requirements')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of requirements enforced for the organization' do
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(403)
      end
    end
  end
end
