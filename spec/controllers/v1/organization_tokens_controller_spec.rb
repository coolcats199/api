# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::OrganizationTokensController, type: :controller do
  describe 'PUT #update' do
    before :each do
      @org = create :organization
    end

    context 'as an Organization Admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org
      end

      it 'recreates the organization token' do
        old_token = @org.token
        norad_put :update, organization_id: @org.slug
        expect(response.status).to eq(200)
        @org.reload
        expect(@org.token).not_to eq(old_token)
      end
    end

    context 'as an Organization Reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org
      end

      it 'should not recreate the organization token' do
        token_value = @org.token
        norad_put :update, organization_id: @org.slug
        expect(response.status).to eq(403)
        @org.reload
        expect(@org.token).to eq(token_value)
      end
    end
  end
end
