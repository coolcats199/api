# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host)
#

require 'rails_helper'

RSpec.describe V1::SecurityTestRepositoriesController, type: :controller do
  before :each do
    @private_repository = create :private_repository
    @public_repository = create :public_repository
  end

  describe 'GET #index' do
    it 'responds with only the names, id and public status' do
      norad_get :index, {}
      expect(response.status).to eq 200
      expect(response).to match_response_schema('security_test_repositories')
      expect(response_body['response'].size).to eq 2
    end
  end

  describe 'GET #show' do
    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @public_repository
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it 'returns all fields for public repositories' do
        norad_get :show, id: @public_repository.id
        expect(response.status).to eq 200
        expect(response).to match_response_schema('security_test_repository')
      end

      it 'returns all fields for private repositories' do
        norad_get :show, id: @public_repository.id
        expect(response.status).to eq 200
        expect(response).to match_response_schema('security_test_repository')
      end
    end

    context 'for an unprivileged user' do
      it 'only shows name and public for unprivileged users' do
        norad_get :show, id: @private_repository.id
        expect(response.status).to eq 200
        expect(response).to match_response_schema('redacted_security_test_repository')
      end

      it 'returns all fields for public repositories' do
        norad_get :show, id: @public_repository.id
        expect(response.status).to eq 200
        expect(response).to match_response_schema('security_test_repository')
      end
    end
  end

  describe 'POST #create' do
    let(:valid_repository_params) { attributes_for(:private_repository) }
    let(:invalid_repository_params) { attributes_for(:private_repository, name: '', password: '') }

    it 'creates a repository with the current user set as admin' do
      norad_post :create, security_test_repository: valid_repository_params
      str = SecurityTestRepository.find(response_body['response']['id'])
      expect(response.status).to eq 200
      expect(response).to match_response_schema('security_test_repository')
      expect(@_current_user.has_role?(:security_test_repository_admin, str)).to eq true
    end

    it 'renders errors for repository' do
      norad_post :create, security_test_repository: invalid_repository_params
      expect(response.status).to eq 422
      expect(response_body['errors']['name']).to include "can't be blank"
      expect(response_body['errors']['password']).to include "can't be blank"
    end
  end

  describe 'PUT #update' do
    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @public_repository
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it 'updates private repositories' do
        norad_put :update, id: @private_repository.id, security_test_repository: { name: 'New Name' }
        expect(response.status).to eq 200
        expect(@private_repository.reload.name).to eq 'New Name'
      end

      it 'renders errors' do
        norad_put :update, id: @private_repository.id, security_test_repository: { name: '', password: '' }
        expect(response.status).to eq 422
        expect(response_body['errors']['name']).to include "can't be blank"
        expect(response_body['errors']['password']).to include "can't be blank"
      end

      it 'updates public repositories' do
        norad_put :update, id: @public_repository.id, security_test_repository: { name: 'New Name' }
        expect(response.status).to eq 200
        expect(@public_repository.reload.name).to eq 'New Name'
      end
    end

    context 'for an unprivileged user' do
      it 'prevents updating private repositories' do
        norad_put :update, id: @private_repository.id, security_test_repository: { name: 'New Name' }
        expect(response.status).to eq 403
        expect(@private_repository.reload.name).not_to eq 'New Name'
      end

      it 'prevents updating public repositories' do
        norad_put :update, id: @public_repository.id, security_test_repository: { name: 'New Name' }
        expect(response.status).to eq 403
        expect(@public_repository.reload.name).not_to eq 'New Name'
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @public_repository
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it 'deletes a private repository' do
        norad_delete :destroy, id: @private_repository.id
        expect(response.status).to eq 204
        expect { @private_repository.reload }.to raise_exception ActiveRecord::RecordNotFound
      end

      it 'deletes a public repository' do
        norad_delete :destroy, id: @public_repository.id
        expect(response.status).to eq 204
        expect { @public_repository.reload }.to raise_exception ActiveRecord::RecordNotFound
      end
    end

    context 'for an unprivileged user' do
      it 'prevents deleting a private repository' do
        expect do
          norad_delete :destroy, id: @private_repository.id
        end.not_to change(SecurityTestRepository, :count)
        expect(response.status).to eq 403
      end

      it 'prevents deleting a public repository' do
        expect do
          norad_delete :destroy, id: @public_repository.id
        end.not_to change(SecurityTestRepository, :count)
        expect(response.status).to eq 403
      end
    end
  end
end
