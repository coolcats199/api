# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::OrganizationScanSchedulesController, type: :controller, with_resque_doubled: true do
  describe 'GET #index' do
    before :each do
      @org = create :organization
      @sched = create :organization_scan_schedule, organization: @org
    end

    context 'as an organization admin' do
      it 'returns the schedules associated with an organization' do
        @_current_user.add_role :organization_admin, @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_scan_schedules')
      end
    end

    context 'as an organization reader' do
      it 'returns the schedules associated with an organization' do
        @_current_user.add_role :organization_reader, @org
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_scan_schedules')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the set of schedules for the organization' do
        norad_get :index, organization_id: @org.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @org = create :organization
      @sched = create :organization_scan_schedule, organization: @org
    end

    context 'as an organization admin' do
      it 'returns the details of a schedule' do
        @_current_user.add_role :organization_admin, @org
        norad_get :show, id: @sched.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_scan_schedule')
      end
    end

    context 'as an organization reader' do
      it 'read the details of a schedule' do
        @_current_user.add_role :organization_reader, @org
        norad_get :show, id: @sched.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_scan_schedule')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific schedule' do
        norad_get :show, id: @sched.id
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
    end

    let(:sched_params) { { organization_scan_schedule: { at: '24:24' } } }

    context 'as an organization admin' do
      it 'creates new schedule for in organization' do
        @_current_user.add_role :organization_admin, @org1
        create_params = sched_params.merge(organization_id: @org1.to_param)
        expect { norad_post :create, create_params }.to change(@org1.scan_schedules, :count).by(1)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('organization_scan_schedule')
      end
    end

    context 'as an organization reader' do
      it 'cannot create new schedule for in organization' do
        @_current_user.add_role :organization_reader, @org1
        create_params = sched_params.merge(organization_id: @org1.to_param)
        expect { norad_post :create, create_params }.to change(@org1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot create new schedule for in organization' do
        @_current_user.add_role :organization_reader, @org2
        create_params = sched_params.merge(organization_id: @org1.to_param)
        expect { norad_post :create, create_params }.to change(@org1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @sched1 = create :organization_scan_schedule, organization: @org1
      @sched2 = create :organization_scan_schedule, organization: @org2
    end

    let(:sched_params) { { organization_scan_schedule: { at: '23:23' } } }

    context 'as an organization admin' do
      it 'updates a schedule for an organization' do
        @_current_user.add_role :organization_admin, @org1
        expect(@sched1.at).to eq('24:24')
        update_params = sched_params.merge(id: @sched1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@sched1.reload.at).to eq('23:23')
      end
    end

    context 'as an organization reader' do
      it 'cannot update a schedule for an organization' do
        @_current_user.add_role :organization_reader, @org1
        update_params = sched_params.merge(id: @sched1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org,' do
      it 'cannot update a schedule for an organization' do
        @_current_user.add_role :organization_admin, @org2
        update_params = sched_params.merge(id: @sched1.to_param)
        norad_put :update, update_params
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @sched1 = create :organization_scan_schedule, organization: @org1
      @sched2 = create :organization_scan_schedule, organization: @org2
    end

    context 'as an organization admin' do
      it 'deletes a schedule for an organization' do
        @_current_user.add_role :organization_admin, @org1
        expect { norad_delete :destroy, id: @sched1 }.to change(@org1.scan_schedules, :count).by(-1)
        expect(response.status).to eq(204)
      end
    end

    context 'as an organization reader' do
      it 'cannot delete a schedule for an organization' do
        @_current_user.add_role :organization_reader, @org1
        expect { norad_delete :destroy, id: @sched1 }.to change(@org1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end

    context 'as an organization outsider with admin in other org' do
      it 'cannot delete a schedule for an organization' do
        @_current_user.add_role :organization_admin, @org2
        expect { norad_delete :destroy, id: @sched1 }.to change(@org1.scan_schedules, :count).by(0)
        expect(response.status).to eq(403)
      end
    end
  end
end
