# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::SecurityContainersController, type: :controller do
  shared_examples_for 'it is accessible by unprivileged users' do
    it 'redacts other private containers' do
      other_repository = create :private_repository
      create :security_container, security_test_repository: other_repository

      norad_get :index, security_test_repository_id: other_repository.to_param
      expect(response).to match_response_schema('redacted_security_containers')
    end

    it 'exposes public containers' do
      norad_get :index, security_test_repository_id: @public_repository.to_param
      expect(response).to match_response_schema('security_containers')
    end
  end

  before :each do
    @private_repository = create :private_repository
    @public_repository = create :public_repository
    @private_container = create :security_container, security_test_repository: @private_repository
    @public_container = create :security_container, security_test_repository: @public_repository
    @official_repository = create :official_repository
    @official_container = create :security_container, security_test_repository: @official_repository
  end

  describe 'POST #create' do
    let(:attributes) { attributes_for :container_with_application_type }

    shared_examples_for 'a non-admin user' do
      it 'should not create for private repositories' do
        norad_post :create, security_test_repository_id: @private_repository.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end

      it 'should not create for public repository' do
        norad_post :create, security_test_repository_id: @public_repository.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end

      it 'should not create for whitelisted repository' do
        create :repository_whitelist_entry, organization: @org, security_test_repository: @private_repository
        norad_post :create, security_test_repository_id: @private_repository.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end
    end

    before :each do
      @org = @_current_user.organizations.first
    end

    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it 'should create for own private repository' do
        norad_post :create, security_test_repository_id: @private_repository.to_param, security_container: attributes
        expect(response).to match_response_schema('security_container')
        expect(response.status).to eq 200
      end

      it 'should create for own whitelisted repository' do
        create :repository_whitelist_entry, organization: @org, security_test_repository: @private_repository
        norad_post :create, security_test_repository_id: @private_repository.to_param, security_container: attributes
        expect(response).to match_response_schema('security_container')
        expect(response.status).to eq 200
      end

      it 'renders errors' do
        invalid_attributes = attributes.merge(prog_args: nil)
        norad_post :create,
                   security_test_repository_id: @private_repository.to_param,
                   security_container: invalid_attributes
        expect(response_body['errors']['prog_args']).to include("can't be blank")
        expect(response.status).to eq 422
      end

      it 'should not create for other repository' do
        norad_post :create, security_test_repository_id: @public_repository.to_param, security_container: attributes
        expect(response.status).to eq 403
      end

      context 'batch functionality' do
        let(:attributes_array) do
          Array.new(5) { attributes_for(:container_with_application_type) }
        end

        it 'should create for own private repository' do
          norad_post :create,
                     security_test_repository_id: @private_repository.to_param,
                     security_containers: attributes_array
          expect(response).to match_response_schema('security_containers')
          expect(response.status).to eq 200
        end

        it 'should create for own whitelisted repository' do
          create :repository_whitelist_entry, organization: @org, security_test_repository: @private_repository
          norad_post :create,
                     security_test_repository_id: @private_repository.to_param,
                     security_containers: attributes_array
          expect(response).to match_response_schema('security_containers')
          expect(response.status).to eq 200
        end

        it 'renders errors' do
          invalid_attributes = attributes_array.map { |attrs| attrs.merge(prog_args: nil) }
          norad_post :create,
                     security_test_repository_id: @private_repository.to_param,
                     security_containers: invalid_attributes
          expect(response_body['errors'].first['prog_args']).to include("can't be blank")
          expect(response.status).to eq 422
        end

        it 'rolls back all records if one has error' do
          invalid_attributes = attributes_array.clone
          invalid_attributes[0][:prog_args] = nil
          expect do
            norad_post :create,
                       security_test_repository_id: @private_repository.to_param,
                       security_containers: invalid_attributes
          end.not_to change(SecurityContainer, :count)
        end
      end
    end

    context 'for an unprivileged user' do
      it_behaves_like 'a non-admin user'
    end

    context 'for readers' do
      before :each do
        @_current_user.add_role :security_test_repository_reader, @private_repository
      end

      it_behaves_like 'a non-admin user'
    end
  end

  describe 'GET #index' do
    shared_examples_for 'a non-admin user' do
      context 'scoped to an organization' do
        before :each do
          @other_repository = create :private_repository
          @other_container = create :security_container, security_test_repository: @other_repository
          @org = @_current_user.organizations.first
        end

        it 'exposes whitelisted and official containers' do
          create :repository_whitelist_entry, organization: @org, security_test_repository: @other_repository
          norad_get :index, organization_id: @org.to_param
          expected_ids = [@other_container, @official_container].map(&:id)
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].map { |r| r['id'] }).to contain_exactly(*expected_ids)
        end

        it 'filters by name if the test_name param is present' do
          create :repository_whitelist_entry, organization: @org, security_test_repository: @other_repository
          norad_get :index, organization_id: @org.to_param, test_name: @official_container.name
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].map { |r| r['id'] }).to contain_exactly(@official_container.id)
        end

        it 'does not return private containers that have not been whitelisted' do
          norad_get :index, organization_id: @org.to_param
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].size).to eq 1
          expect(response_body['response'].map { |r| r['id'] }).not_to include(@other_container.id)
        end
      end

      context 'scoped to a test repository' do
        it_should_behave_like 'it is accessible by unprivileged users'
      end

      context 'unscoped' do
        it_should_behave_like 'an unscoped security containers index lookup'
      end
    end

    shared_examples_for 'an unscoped security containers index lookup' do
      before(:each) do
        2.times do
          create :security_container, security_test_repository: @official_repository
        end
      end

      it 'exposes all official security containers' do
        norad_get :index, {}
        expect(response).to match_response_schema('security_containers')
        expect(response_body['response'].map { |r| r['id'] })
          .to contain_exactly(*@official_repository.security_containers.pluck(:id))
      end

      it 'filters by name if the test_name param is present' do
        test = @official_repository.security_containers.sample
        norad_get :index, test_name: test.name
        expect(response).to match_response_schema('security_containers')
        expect(response_body['response'].map { |r| r['id'] })
          .to contain_exactly(test.id)
      end
    end

    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      context 'scoped to an organization' do
        before :each do
          @other_repository = create :private_repository
          @other_container = create :security_container, security_test_repository: @other_repository
          @org = @_current_user.organizations.first
        end

        it 'exposes whitelisted and official containers' do
          create :repository_whitelist_entry, organization: @org, security_test_repository: @other_repository
          norad_get :index, organization_id: @org.to_param
          expected_ids = [@other_container, @official_container].map(&:id)
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].map { |r| r['id'] }).to contain_exactly(*expected_ids)
        end

        it 'filters by name if the test_name param is present' do
          create :repository_whitelist_entry, organization: @org, security_test_repository: @other_repository
          norad_get :index, organization_id: @org.to_param, test_name: @official_container.name
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].map { |r| r['id'] }).to contain_exactly(@official_container.id)
        end

        it 'does not return private containers that have not been whitelisted' do
          norad_get :index, organization_id: @org.to_param
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].size).to eq 1
          expect(response_body['response'].map { |r| r['id'] }).not_to include(@other_container.id)
        end
      end

      context 'scoped to a test repository' do
        it_should_behave_like 'it is accessible by unprivileged users'

        it 'exposes own private containers' do
          norad_get :index, security_test_repository_id: @private_repository.to_param
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].map { |r| r['id'] }).to contain_exactly(@private_container.id)
        end

        it 'filters by test name if test_name param is present' do
          named_container = create(:security_container, security_test_repository: @private_repository)
          norad_get :index, security_test_repository_id: @private_repository.to_param, test_name: named_container.name
          expect(response).to match_response_schema('security_containers')
          expect(response_body['response'].map { |r| r['id'] }).to contain_exactly(named_container.id)
        end
      end

      context 'unscoped' do
        it_should_behave_like 'an unscoped security containers index lookup'
      end
    end

    context 'for a reader' do
      before :each do
        @_current_user.add_role :security_test_repository_reader, @private_repository
      end

      it_behaves_like 'a non-admin user'
    end

    context 'for unprivileged users' do
      it_behaves_like 'a non-admin user'
    end
  end

  describe 'GET #show' do
    shared_examples_for 'an unprivileged user' do
      it 'exposes an individual container in the whitelist' do
        other_repository = create :private_repository
        other_container = create :security_container, security_test_repository: other_repository
        org = @_current_user.organizations.first
        create :repository_whitelist_entry, organization: org, security_test_repository: other_repository
        norad_get :show, id: other_container.to_param
        expect(response).to match_response_schema('security_container')
      end

      it 'redacts an individual container belonging to a private repository' do
        norad_get :show, id: @private_container.to_param
        expect(response).to match_response_schema('redacted_security_container')
      end

      it 'exposes an individual container belonging to a public repository' do
        norad_get :show, id: @public_container.to_param
        expect(response).to match_response_schema('security_container')
      end
    end

    shared_examples_for 'a privileged user' do
      it 'exposes an individual container in the whitelist' do
        other_repository = create :private_repository
        other_container = create :security_container, security_test_repository: other_repository
        org = @_current_user.organizations.first
        create :repository_whitelist_entry, organization: org, security_test_repository: other_repository
        norad_get :show, id: other_container.to_param
        expect(response).to match_response_schema('security_container')
      end

      it 'exposes an individual container belonging to a private repository' do
        norad_get :show, id: @private_container.to_param
        expect(response).to match_response_schema('security_container')
      end

      it 'exposes an individual container belonging to a public repository' do
        norad_get :show, id: @public_container.to_param
        expect(response).to match_response_schema('security_container')
      end
    end

    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it_behaves_like 'a privileged user'
    end

    context 'for a reader' do
      before :each do
        @_current_user.add_role :security_test_repository_reader, @private_repository
      end

      it_behaves_like 'a privileged user'
    end

    context 'for unprivileged users' do
      it_behaves_like 'an unprivileged user'
    end
  end

  describe 'PUT #update' do
    shared_examples_for 'a non-admin user' do
      it 'should not update for private repositories' do
        norad_put :update, id: @private_container.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end

      it 'should not update for public repository' do
        norad_put :update, id: @public_container.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end

      it 'should not update for whitelisted repository' do
        create :repository_whitelist_entry, organization: @org, security_test_repository: @private_repository
        norad_put :update, id: @public_container.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end
    end

    before :each do
      @attributes = attributes_for :container_with_application_type
      @org = @_current_user.organizations.first
    end

    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it 'should update for own private repository' do
        norad_put :update, id: @private_container.to_param, security_container: @attributes
        expect(response).to match_response_schema('security_container')
        expect(response.status).to eq 200
      end

      it 'renders errors' do
        @attributes[:prog_args] = nil
        norad_put :update, id: @private_container.to_param, security_container: @attributes
        expect(response_body['errors']['prog_args']).to include("can't be blank")
        expect(response.status).to eq 422
      end

      it 'should not update for other repository' do
        norad_put :update, id: @public_container.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end

      it 'should not create for whitelisted repository' do
        create :repository_whitelist_entry, organization: @org, security_test_repository: @private_repository
        norad_put :update, id: @public_container.to_param, security_container: @attributes
        expect(response.status).to eq 403
      end
    end

    context 'for a reader' do
      before :each do
        @_current_user.add_role :security_test_repository_reader, @private_repository
      end

      it_behaves_like 'a non-admin user'
    end

    context 'for an unprivileged user' do
      it_behaves_like 'a non-admin user'
    end
  end

  describe 'DELETE #destroy' do
    shared_examples_for 'a non-admin user' do
      it 'does not delete other private container' do
        norad_delete :destroy, id: @private_container
        expect(response.status).to eq 403
      end

      it 'does not delete other public container' do
        norad_delete :destroy, id: @public_container
        expect(response.status).to eq 403
      end
    end

    context 'for an admin' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
      end

      it 'deletes for own private container' do
        norad_delete :destroy, id: @private_container
        expect(response.status).to eq 204
      end

      it 'does not delete other public container' do
        norad_delete :destroy, id: @public_container
        expect(response.status).to eq 403
      end
    end

    context 'for a reader' do
      before :each do
        @_current_user.add_role :security_test_repository_reader, @private_repository
      end

      it_behaves_like 'a non-admin user'
    end

    context 'for an unprivileged user' do
      it_behaves_like 'a non-admin user'
    end
  end
end
