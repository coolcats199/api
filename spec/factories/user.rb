# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:uid) { |n| "factoryuser_#{n}" }
    email { "#{uid || 'user'}@cisco.com" }
    firstname 'factory-first-name'
    lastname 'factory-last-name'

    factory :user_with_password do
      before(:create) do |user|
        create :local_authentication_method, user: user
      end
    end

    factory :user_with_sso do
      before(:create) do |user|
        create :reverse_proxy_authentication_method, user: user
      end
    end
  end
end
