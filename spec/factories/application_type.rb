# frozen_string_literal: true

# == Schema Information
#
# Table name: application_types
#
#  id                 :integer          not null, primary key
#  port               :integer          not null
#  name               :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  transport_protocol :integer          default("tcp"), not null
#
# Indexes
#
#  index_application_types_on_port  (port)
#

FactoryBot.define do
  factory :application_type do
    port 22
    name 'ssh'
    transport_protocol 'tcp'
  end
end
