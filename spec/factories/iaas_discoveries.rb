# frozen_string_literal: true

# == Schema Information
#
# Table name: iaas_discoveries
#
#  id                    :integer          not null, primary key
#  iaas_configuration_id :integer          not null
#  state                 :integer          default("pending"), not null
#  error_message         :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_iaas_discoveries_on_ip_state       (iaas_configuration_id,state) UNIQUE WHERE (state = 1)
#  index_iaas_discoveries_on_pending_state  (iaas_configuration_id,state) UNIQUE WHERE (state = 0)
#
# Foreign Keys
#
#  fk_rails_62a3d3b299  (iaas_configuration_id => iaas_configurations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :iaas_discovery do
    iaas_configuration
    state 0
    error_message nil
  end
end
