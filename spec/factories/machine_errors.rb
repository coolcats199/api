# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :machine_error do
    organization
    type 'MachineError'
    association :errable, factory: :machine

    factory :machine_error_with_latest_check do
      after :build do |error|
        error.machine_connectivity_check =
          create(:machine_connectivity_check, machine: error.machine)
      end

      factory :unable_to_ssh_to_machine_error, class: UnableToSshToMachineError do
        type 'UnableToSshToMachineError'
      end

      factory :unable_to_ping_machine_error, class: UnableToPingMachineError do
        type 'UnableToPingMachineError'
      end
    end
  end
end
