# frozen_string_literal: true

# == Schema Information
#
# Table name: infosec_export_queue_configurations
#
#  id                     :integer          not null, primary key
#  ctsm_id                :string
#  result_export_queue_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_ieq_configs_on_result_export_queue_id  (result_export_queue_id)
#
# Foreign Keys
#
#  fk_rails_e9e9747506  (result_export_queue_id => result_export_queues.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :infosec_export_queue_configuration do
    association :infosec_export_queue
    sequence(:ctsm_id) { |n| "CTSM-#{n}" }
    created_at DateTime.now.iso8601
    updated_at DateTime.now.iso8601
  end
end
