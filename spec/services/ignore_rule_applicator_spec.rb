# frozen_string_literal: true

require 'rails_helper'

RSpec.describe IgnoreRuleApplicator do
  describe 'instance methods' do
    let(:subject) { described_class.new(signature, ignore_scope) }
    let(:signature) { 'anything' }
    let(:ignore_scope) { double }
    let(:results_klass) { class_double('Result') }

    before :each do
      allow(ignore_scope).to receive(:latest_results).and_return(results_klass)
      allow(results_klass).to receive(:with_signature).and_return(results_klass)
    end

    describe 'the #apply method' do
      it 'instructs the Result class to ignore a set of results' do
        expect(results_klass).to receive(:ignore)
        subject.apply
      end

      it 'scopes the results by signature' do
        allow(results_klass).to receive(:ignore)
        expect(results_klass).to receive(:with_signature)
        subject.apply
      end
    end

    describe 'the #undo method' do
      it 'instructs the Result class to ignore a set of results' do
        expect(results_klass).to receive(:unignore)
        subject.undo
      end

      it 'scopes the results by signature' do
        allow(results_klass).to receive(:unignore)
        expect(results_klass).to receive(:with_signature)
        subject.undo
      end
    end
  end
end
