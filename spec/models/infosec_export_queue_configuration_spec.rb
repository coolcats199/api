# frozen_string_literal: true

# == Schema Information
#
# Table name: infosec_export_queue_configurations
#
#  id                     :integer          not null, primary key
#  ctsm_id                :string
#  result_export_queue_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_ieq_configs_on_result_export_queue_id  (result_export_queue_id)
#
# Foreign Keys
#
#  fk_rails_e9e9747506  (result_export_queue_id => result_export_queues.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe InfosecExportQueue, type: :model do
  it 'has a user-friendly error message if an invalid CTSM_ID is passed in' do
    record = build(:infosec_export_queue_configuration, ctsm_id: 'invalid')
    record.valid?

    assert_equal(
      ['CTSM ID has to start with "CTSM-" and end with a number'],
      record.errors.full_messages
    )
  end
end
