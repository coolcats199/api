# frozen_string_literal: true

# == Schema Information
#
# Table name: scan_containers
#
#  id                    :integer          not null, primary key
#  docker_command_id     :integer          not null
#  security_container_id :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_scan_containers_on_d_c_id_and_s_c_id  (docker_command_id,security_container_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_214538d76e  (security_container_id => security_containers.id) ON DELETE => cascade
#  fk_rails_a9e9cc8192  (docker_command_id => docker_commands.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe ScanContainer, type: :model do
  context 'when validating' do
    subject { build(:scan_container) }
    it { should validate_presence_of(:docker_command) }
    it { should validate_presence_of(:security_container) }

    context 'when updating' do
      let(:dc) { create :docker_command, commandable: create(:organization) }
      let(:sc) { dc.scan_containers.first }

      it 'prevents changing associations' do
        sc.docker_command = create :docker_command, commandable: create(:organization)
        expect(sc).not_to be_valid
        expect(sc.errors[:base]).to include("Can't change associated docker_command or security_container")

        sc.security_container = create :security_container
        expect(sc).not_to be_valid
        expect(sc.errors[:base]).to include("Can't change associated docker_command or security_container")
      end
    end

    context 'when creating' do
      context 'for external containers' do
        context 'in the org whitelist' do
          let(:dc) { build :docker_command_from_unofficial_repository, commandable: create(:organization) }

          it 'disallows scan with whitelisted external containers when organization has no relay' do
            error_msg = 'from unofficial repositories when no Relay configured'
            expect(dc).not_to be_valid
            expect(dc.errors['scan_containers.tests']).to include(error_msg)
          end

          it 'allows scan with whitelisted external containers when relay is configured' do
            allow_any_instance_of(Organization).to receive(:docker_relay_queue).and_return('a queue')
            expect(dc).to be_valid
          end
        end

        context 'outside the org whitelist' do
          let(:dc) do
            build :docker_command,
                  commandable: create(:organization),
                  scan_containers_attributes: [{ security_container_id: create(:private_container).id }]
          end

          shared_examples 'respects the whitelist' do
            it 'disallows scan' do
              expect(dc).not_to be_valid
              expect(dc.errors['scan_containers.tests']).to include('have not been whitelisted for this organization')
            end
          end

          context 'with relay configured' do
            before { allow_any_instance_of(Organization).to receive(:docker_relay_queue).and_return('a queue') }
            it_behaves_like 'respects the whitelist'
          end

          context 'without relay configured' do
            it_behaves_like 'respects the whitelist'
          end
        end
      end

      context 'for norad-hosted containers' do
        let(:dc) { build :docker_command, commandable: create(:organization) }

        shared_examples 'it allows scan' do
          it do
            expect(dc).to be_valid
          end
        end

        context 'with relay configured' do
          before { allow_any_instance_of(Organization).to receive(:docker_relay_queue).and_return('a queue') }
          it_behaves_like 'it allows scan'
        end

        context 'without relay configured' do
          it_behaves_like 'it allows scan'
        end
      end
    end
  end
end
