# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'support/organization_error_spec_helpers'
require 'models/shared_examples/ssh_organization_errors'

RSpec.describe KeyPairAssignmentWithoutSshServiceError, type: :model do
  include OrganizationErrorSpecHelpers

  describe '::check method' do
    let(:assignment) { create :ssh_key_pair_assignment }

    describe 'error creation' do
      it 'adds error when key pair assignment exists without ssh service' do
        machine = assignment.machine.reload
        org = machine.organization
        expected_msg = "You have added an SSH Key Pair to machine #{machine.name} but no SSH Service is registered for"\
          ' this machine.'
        described_class.check(org, subject: machine)
        expect(described_errors(org).first.message).to eq expected_msg
      end
    end

    describe 'error removal' do
      let(:org) { assignment.ssh_key_pair.organization }
      let(:machine) { assignment.machine }

      it_behaves_like 'SSH Organization Error class' do
        let(:error) { create :key_pair_assignment_without_ssh_service_error }
      end

      it 'avoids cached associations during callbacks' do
        expect(described_errors(org)).not_to be_empty

        # Ensure errors are removed even when we build parent models of the STI child
        service = build :service, machine: machine, type: 'SshService', encryption_type: 2, port_type: 0

        expect { service.save! }.to change(OrganizationError, :count).by(-1)
        expect(described_errors(org)).to be_empty
      end
    end
  end
end
