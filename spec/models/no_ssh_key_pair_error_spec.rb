# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/organization_errors'

RSpec.describe NoSshKeyPairError, type: :model do
  it_behaves_like 'an Organization Error class'

  before :each do
    allow_any_instance_of(DockerRelay).to receive(:check_for_organization_errors)
    allow_any_instance_of(SshKeyPair).to receive(:check_for_organization_errors)
    allow_any_instance_of(SecurityContainerConfig).to receive(:check_for_organization_errors)
    allow_any_instance_of(Machine).to receive(:check_for_organization_errors)
    @organization = create :organization
  end

  def errors_for_org(org = @organization)
    org.organization_errors.where(type: described_class.name).reload
  end

  def create_authenticated_test_config(opts)
    create :security_container_config, { enabled_outside_of_requirement: true, security_container: @test }.merge(opts)
  end

  def check_for_error(org = @organization)
    described_class.check org
  end

  describe '::check method' do
    context 'error creation' do
      before :each do
        @test = create :security_container, test_types: ['authenticated']
      end

      context 'when removing the only SSH Key Pair from an Organization' do
        before :each do
          @keypair = create :ssh_key_pair, organization: @organization
        end

        context 'when not using relay ssh key' do
          it 'creates an error when authenticated tests are enabled at the organization level' do
            create_authenticated_test_config(configurable: @organization)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            @organization.ssh_key_pairs.destroy_all
            check_for_error
            expect(errors_for_org.empty?).to eq(false)
          end

          it 'creates an error when authenticated tests are enabled for a machine in the organization' do
            create_authenticated_test_config(configurable: create(:machine, organization: @organization))
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            @organization.ssh_key_pairs.destroy_all
            check_for_error
            expect(errors_for_org.empty?).to eq(false)
          end
        end

        context 'when using relay ssh key' do
          before :each do
            @organization.configuration.use_relay_ssh_key = true
            relay = create :docker_relay, organization: @organization
            relay.verified = true
            relay.save
            @organization.save
          end

          it 'does not create an error when authenticated tests are enabled at the organization level' do
            create_authenticated_test_config(configurable: @organization)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            @organization.ssh_key_pairs.destroy_all
            check_for_error
            expect(errors_for_org.empty?).to eq(true)
          end

          it 'does not create an error when authenticated tests are enabled for a machine in the organization' do
            create_authenticated_test_config(configurable: create(:machine, organization: @organization))
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            @organization.ssh_key_pairs.destroy_all
            check_for_error
            expect(errors_for_org.empty?).to eq(true)
          end
        end
      end

      context 'when watch for organization errors is triggered' do
        before :each do
          expect(@organization.ssh_key_pairs.empty?).to eq(true)
        end

        context 'when not using relay ssh key' do
          it 'creates an error if a config is created for an organization' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            create_authenticated_test_config(configurable: @organization)
            check_for_error
            expect(errors_for_org.empty?).to eq(false)
          end

          it 'creates an error if a config is created for a machine' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            create_authenticated_test_config(configurable: create(:machine, organization: @organization))
            check_for_error
            expect(errors_for_org.empty?).to eq(false)
          end

          it 'creates an error if a config is enabled for an organization' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config = create_authenticated_test_config(configurable: @organization)
            config.update_column(:enabled_outside_of_requirement, false)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config.update_column(:enabled_outside_of_requirement, true)
            check_for_error
            expect(errors_for_org.empty?).to eq(false)
          end

          it 'creates an error if a config is enabled for a machine' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config = create_authenticated_test_config(
              configurable: create(:machine, organization: @organization)
            )
            config.update_column(:enabled_outside_of_requirement, false)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config.update_column(:enabled_outside_of_requirement, true)
            check_for_error
            expect(errors_for_org.empty?).to eq(false)
          end
        end

        context 'when using relay ssh key' do
          before :each do
            @organization.configuration.use_relay_ssh_key = true
            relay = create :docker_relay, organization: @organization
            relay.verified = true
            relay.save
            @organization.save
          end

          it 'does not create an error if a config is created for an organization' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            create_authenticated_test_config(configurable: @organization)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)
          end

          it 'does not create an error if a config is created for a machine' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            create_authenticated_test_config(configurable: create(:machine, organization: @organization))
            check_for_error
            expect(errors_for_org.empty?).to eq(true)
          end

          it 'does not create an error if a config is enabled for an organization' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config = create_authenticated_test_config(configurable: @organization)
            config.update_column(:enabled_outside_of_requirement, false)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config.update_column(:enabled_outside_of_requirement, true)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)
          end

          it 'does not create an error if a config is enabled for a machine' do
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config = create_authenticated_test_config(
              configurable: create(:machine, organization: @organization)
            )
            config.update_column(:enabled_outside_of_requirement, false)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)

            config.update_column(:enabled_outside_of_requirement, true)
            check_for_error
            expect(errors_for_org.empty?).to eq(true)
          end
        end
      end
    end

    context 'error removal' do
      before :each do
        @test = create :security_container, test_types: ['authenticated']
      end

      context 'when an SSH Key Pair is added' do
        it 'removes the error when organization level config exists' do
          create_authenticated_test_config(configurable: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          create :ssh_key_pair, organization: @organization
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config exists' do
          create_authenticated_test_config(configurable: create(:machine, organization: @organization))
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          create :ssh_key_pair, organization: @organization
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end

      context 'when a relay using SSH key pair is added' do
        def add_relay
          @organization.configuration.use_relay_ssh_key = true
          relay = create :docker_relay, organization: @organization
          relay.verified = true
          relay.save
          @organization.save
        end

        it 'removes the error when organization level config exists' do
          create_authenticated_test_config(configurable: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          add_relay
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config exists' do
          create_authenticated_test_config(configurable: create(:machine, organization: @organization))
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          add_relay
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end

      context 'when test configs are removed' do
        it 'removes the error when an organization level config is destroyed' do
          config = create_authenticated_test_config(configurable: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.destroy
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config is destroyed' do
          config = create_authenticated_test_config(
            configurable: create(:machine, organization: @organization)
          )
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.destroy
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end

      context 'when test configs are disabled' do
        it 'removes the error when an organization level config is disabled' do
          config = create_authenticated_test_config(configurable: @organization)
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.update_column(:enabled_outside_of_requirement, false)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end

        it 'removes the error when a machine level config is disabled' do
          config = create_authenticated_test_config(
            configurable: create(:machine, organization: @organization)
          )
          check_for_error
          expect(errors_for_org.empty?).to eq(false)

          config.update_column(:enabled_outside_of_requirement, false)
          check_for_error
          expect(errors_for_org.empty?).to eq(true)
        end
      end
    end
  end
end
