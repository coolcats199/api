# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/machine_error'

RSpec.describe UnableToSshToMachineError, type: :model do
  it_behaves_like 'a Machine Error class'

  context '#message method' do
    let(:machine) { create(:ssh_key_pair_assignment).machine.reload }
    let(:error) { build(:unable_to_ssh_to_machine_error, errable: machine) }

    context 'when using key pair' do
      it 'contextualizes to ssh key pair' do
        expect(error.message)
          .to eq "Unable to ssh to machine #{machine.name} using SSH Key Pair #{machine.ssh_key_pair.name}"
      end
    end

    context 'when using relay key' do
      it 'contextualizes to relay key' do
        org = error.machine.organization
        org.configuration.update!(use_relay_ssh_key: true)
        relay = create(:docker_relay, organization: org)
        relay.verified = true
        relay.save!
        expect(error.message)
          .to eq "Unable to ssh to machine #{machine.name} using its Organization's Relay SSH key"
      end
    end

    context 'when key pair is missing' do
      it 'omits key' do
        # Don't run callbacks since this will remove the error
        error.machine.ssh_key_pair_assignment.delete

        expect(error.message).to eq "Unable to ssh to machine #{machine.name}"
      end
    end
  end
end
