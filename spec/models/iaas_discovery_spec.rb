# frozen_string_literal: true

# == Schema Information
#
# Table name: iaas_discoveries
#
#  id                    :integer          not null, primary key
#  iaas_configuration_id :integer          not null
#  state                 :integer          default("pending"), not null
#  error_message         :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
# Indexes
#
#  index_iaas_discoveries_on_ip_state       (iaas_configuration_id,state) UNIQUE WHERE (state = 1)
#  index_iaas_discoveries_on_pending_state  (iaas_configuration_id,state) UNIQUE WHERE (state = 0)
#
# Foreign Keys
#
#  fk_rails_62a3d3b299  (iaas_configuration_id => iaas_configurations.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe IaasDiscovery, type: :model do
  context 'when validating' do
    it { validate_presence_of(:iaas_configuration) }

    it 'validates the absence of error_message unless the state is failed' do
      p = build :iaas_discovery, state: IaasDiscovery::PENDING_STATE, error_message: 'abc'
      ip = build :iaas_discovery, state: IaasDiscovery::PENDING_STATE, error_message: 'abc'
      c = build :iaas_discovery, state: 2, error_message: 'abc'
      f = build :iaas_discovery, state: 3, error_message: 'abc'
      expect(p.valid?).to be(false)
      expect(p.errors.messages[:error_message]).to include('must be blank')
      expect(ip.valid?).to be(false)
      expect(ip.errors.messages[:error_message]).to include('must be blank')
      expect(c.valid?).to be(false)
      expect(c.errors.messages[:error_message]).to include('must be blank')
      expect(f.valid?).to be(true)
    end

    it 'validates the presence of error_message if the state is failed' do
      p = build :iaas_discovery, state: IaasDiscovery::PENDING_STATE, error_message: ''
      ip = build :iaas_discovery, state: IaasDiscovery::PENDING_STATE, error_message: ''
      c = build :iaas_discovery, state: 2, error_message: ''
      f = build :iaas_discovery, state: 3, error_message: ''
      expect(p.valid?).to be(true)
      expect(ip.valid?).to be(true)
      expect(c.valid?).to be(true)
      expect(f.valid?).to be(false)
      expect(f.errors.messages[:error_message]).to include("can't be blank")
    end
  end

  context 'database constraints' do
    before :each do
      @config = create :iaas_configuration
    end

    it 'only allows one pending discovery per configuration' do
      expect do
        create :iaas_discovery, state: IaasDiscovery::PENDING_STATE, iaas_configuration: @config
      end.to_not raise_error

      expect do
        create :iaas_discovery, state: IaasDiscovery::PENDING_STATE, iaas_configuration: @config
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end

    it 'only allows one in progress discovery per configuration' do
      expect do
        create :iaas_discovery, state: IaasDiscovery::IN_PROGRESS_STATE, iaas_configuration: @config
      end.to_not raise_error

      expect do
        create :iaas_discovery, state: IaasDiscovery::IN_PROGRESS_STATE, iaas_configuration: @config
      end.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end
end
