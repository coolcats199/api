# frozen_string_literal: true

require 'rails_helper'

describe SecurityTestRepositoryAuthorizer, type: :authorizer do
  let(:public_attributes) { %i[name] }
  let(:private_attributes) { %i[id host name public username password_encrypted created_at updated_at] }

  before :each do
    @admin_user_private = create :user
    @admin_user_public = create :user
    @reader_user_private = create :user
    @reader_user_public = create :user
    @unprivileged_user = create :user
    @private_repository = create :private_repository
    @public_repository = create :public_repository
    @admin_user_private.add_role :security_test_repository_admin, @private_repository
    @admin_user_public.add_role :security_test_repository_admin, @public_repository
    @reader_user_private.add_role :security_test_repository_reader, @private_repository
    @reader_user_public.add_role :security_test_repository_reader, @public_repository
  end

  context 'security_test_repository action' do
    it 'try to create' do
      expect(SecurityTestRepository.authorizer).to be_creatable_by(@admin_user_private)
      expect(SecurityTestRepository.authorizer).to be_creatable_by(@admin_user_public)
      expect(SecurityTestRepository.authorizer).to be_creatable_by(@unprivileged_user)
      expect(SecurityTestRepository.authorizer).to be_creatable_by(@reader_user_private)
      expect(SecurityTestRepository.authorizer).to be_creatable_by(@reader_user_public)
    end

    it 'try to delete' do
      expect(@private_repository.authorizer).to be_deletable_by(@admin_user_private)
      expect(@private_repository.authorizer).not_to be_deletable_by(@admin_user_public)
      expect(@private_repository.authorizer).not_to be_deletable_by(@unprivileged_user)
      expect(@private_repository.authorizer).not_to be_deletable_by(@reader_user_private)
      expect(@private_repository.authorizer).not_to be_deletable_by(@reader_user_public)

      expect(@public_repository.authorizer).to be_deletable_by(@admin_user_public)
      expect(@public_repository.authorizer).not_to be_deletable_by(@admin_user_private)
      expect(@public_repository.authorizer).not_to be_deletable_by(@unprivileged_user)
      expect(@public_repository.authorizer).not_to be_deletable_by(@reader_user_private)
      expect(@public_repository.authorizer).not_to be_deletable_by(@reader_user_public)
    end

    it 'try to update' do
      expect(@private_repository.authorizer).to be_updatable_by(@admin_user_private)
      expect(@private_repository.authorizer).not_to be_updatable_by(@admin_user_public)
      expect(@private_repository.authorizer).not_to be_updatable_by(@unprivileged_user)
      expect(@private_repository.authorizer).not_to be_deletable_by(@reader_user_private)
      expect(@private_repository.authorizer).not_to be_deletable_by(@reader_user_public)

      expect(@public_repository.authorizer).to be_updatable_by(@admin_user_public)
      expect(@public_repository.authorizer).not_to be_updatable_by(@admin_user_private)
      expect(@public_repository.authorizer).not_to be_updatable_by(@unprivileged_user)
      expect(@public_repository.authorizer).not_to be_deletable_by(@reader_user_private)
      expect(@public_repository.authorizer).not_to be_deletable_by(@reader_user_public)
    end

    it 'try to read from class level' do
      expect(SecurityTestRepository.authorizer).to be_readable_by(@admin_user_private)
      expect(SecurityTestRepository.authorizer).to be_readable_by(@admin_user_public)
      expect(SecurityTestRepository.authorizer).to be_readable_by(@unprivileged_user)
      expect(SecurityTestRepository.authorizer).to be_readable_by(@reader_user_private)
      expect(SecurityTestRepository.authorizer).to be_readable_by(@reader_user_public)
    end

    it 'try to read from instance level' do
      expect(@private_repository.authorizer).to be_readable_by(@admin_user_private)
      expect(@private_repository.authorizer).to be_readable_by(@admin_user_public)
      expect(@private_repository.authorizer).to be_readable_by(@unprivileged_user)
      expect(@private_repository.authorizer).to be_readable_by(@reader_user_private)
      expect(@private_repository.authorizer).to be_readable_by(@reader_user_public)

      expect(@public_repository.authorizer).to be_readable_by(@admin_user_private)
      expect(@public_repository.authorizer).to be_readable_by(@admin_user_public)
      expect(@public_repository.authorizer).to be_readable_by(@unprivileged_user)
      expect(@public_repository.authorizer).to be_readable_by(@reader_user_private)
      expect(@public_repository.authorizer).to be_readable_by(@reader_user_public)
    end
  end
end
