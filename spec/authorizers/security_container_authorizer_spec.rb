# frozen_string_literal: true

require 'rails_helper'

describe SecurityContainerAuthorizer, type: :authorizer do
  let(:public_attributes) { %i[name] }
  let(:private_attributes) { %i[id host name public username password_encrypted created_at updated_at] }

  before :each do
    @admin_user_private = create :user
    @admin_user_public = create :user
    @reader_user_private = create :user
    @reader_user_public = create :user
    @unprivileged_user = create :user
    @private_repository = create :private_repository
    @public_repository = create :public_repository
    @private_container = create :security_container, security_test_repository: @private_repository
    @public_container = create :security_container, security_test_repository: @public_repository
    @admin_user_private.add_role :security_test_repository_admin, @private_repository
    @admin_user_public.add_role :security_test_repository_admin, @public_repository
    @reader_user_private.add_role :security_test_repository_reader, @private_repository
    @reader_user_public.add_role :security_test_repository_reader, @public_repository
  end

  context 'security_container action' do
    it 'try to create' do
      expect(SecurityContainer.authorizer).to be_creatable_by(@admin_user_private, in: @private_repository)
      expect(SecurityContainer.authorizer).to be_creatable_by(@admin_user_public, in: @public_repository)

      expect(SecurityContainer.authorizer).not_to be_creatable_by(@admin_user_private, in: @public_repository)
      expect(SecurityContainer.authorizer).not_to be_creatable_by(@admin_user_public, in: @private_repository)

      expect(SecurityContainer.authorizer).not_to be_creatable_by(@unprivileged_user, in: @private_repository)
      expect(SecurityContainer.authorizer).not_to be_creatable_by(@unprivileged_user, in: @public_repository)

      expect(SecurityContainer.authorizer).not_to be_creatable_by(@reader_user_private, in: @private_repository)
      expect(SecurityContainer.authorizer).not_to be_creatable_by(@reader_user_public, in: @public_repository)
    end

    it 'try to delete' do
      expect(@private_container.authorizer).to be_deletable_by(@admin_user_private)
      expect(@private_container.authorizer).not_to be_deletable_by(@admin_user_public)
      expect(@private_container.authorizer).not_to be_deletable_by(@unprivileged_user)
      expect(@private_container.authorizer).not_to be_deletable_by(@reader_user_private)

      expect(@public_container.authorizer).to be_deletable_by(@admin_user_public)
      expect(@public_container.authorizer).not_to be_deletable_by(@admin_user_private)
      expect(@public_container.authorizer).not_to be_deletable_by(@unprivileged_user)
      expect(@public_container.authorizer).not_to be_deletable_by(@reader_user_public)
    end

    it 'try to update' do
      expect(@private_container.authorizer).to be_updatable_by(@admin_user_private)
      expect(@private_container.authorizer).not_to be_updatable_by(@admin_user_public)
      expect(@private_container.authorizer).not_to be_updatable_by(@unprivileged_user)
      expect(@private_container.authorizer).not_to be_updatable_by(@reader_user_private)

      expect(@public_container.authorizer).to be_updatable_by(@admin_user_public)
      expect(@public_container.authorizer).not_to be_updatable_by(@admin_user_private)
      expect(@public_container.authorizer).not_to be_updatable_by(@unprivileged_user)
      expect(@public_container.authorizer).not_to be_updatable_by(@reader_user_public)
    end

    it 'try to read from class level' do
      expect(SecurityContainer.authorizer).to be_readable_by(@admin_user_private)
      expect(SecurityContainer.authorizer).to be_readable_by(@admin_user_public)
      expect(SecurityContainer.authorizer).to be_readable_by(@reader_user_private)
      expect(SecurityContainer.authorizer).to be_readable_by(@reader_user_public)
      expect(SecurityContainer.authorizer).to be_readable_by(@unprivileged_user)
    end

    it 'try to read from instance level' do
      expect(@private_container.authorizer).to be_readable_by(@admin_user_private)
      expect(@private_container.authorizer).to be_readable_by(@admin_user_public)
      expect(@private_container.authorizer).to be_readable_by(@reader_user_private)
      expect(@private_container.authorizer).to be_readable_by(@reader_user_public)
      expect(@private_container.authorizer).to be_readable_by(@unprivileged_user)

      expect(@public_container.authorizer).to be_readable_by(@admin_user_private)
      expect(@public_container.authorizer).to be_readable_by(@admin_user_public)
      expect(@public_container.authorizer).to be_readable_by(@reader_user_private)
      expect(@public_container.authorizer).to be_readable_by(@reader_user_public)
      expect(@public_container.authorizer).to be_readable_by(@unprivileged_user)
    end
  end
end
