# frozen_string_literal: true

require 'rails_helper'

describe RepositoryMemberAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @reader_user = create :user
    @other_admin_user = create :user
    @repository = create :private_repository
    @member = create :repository_reader, user: @reader_user, security_test_repository: @repository
    create :repository_admin, user: @other_admin_user, security_test_repository: @repository
    create :repository_admin, user: @admin_user, security_test_repository: @repository
    @unprivileged_user = create :user
  end

  context 'security_test_repository action' do
    it 'try to create' do
      expect(RepositoryMember.authorizer).to be_creatable_by(@other_admin_user, in: @repository)
      expect(RepositoryMember.authorizer).to be_creatable_by(@admin_user, in: @repository)
      expect(RepositoryMember.authorizer).not_to be_creatable_by(@unprivileged_user, in: @repository)
      expect(RepositoryMember.authorizer).not_to be_creatable_by(@reader_user, in: @repository)
    end

    it 'try to delete' do
      expect(@member.authorizer).to be_deletable_by(@other_admin_user)
      expect(@member.authorizer).to be_deletable_by(@admin_user)
      expect(@member.authorizer).not_to be_deletable_by(@unprivileged_user)
      expect(@member.authorizer).not_to be_deletable_by(@reader_user)
    end

    it 'try to read from class level' do
      expect(RepositoryMember.authorizer).to be_readable_by(@admin_user, in: @repository)
      expect(RepositoryMember.authorizer).to be_readable_by(@other_admin_user, in: @repository)
      expect(RepositoryMember.authorizer).not_to be_readable_by(@reader_user, in: @repository)
      expect(RepositoryMember.authorizer).not_to be_readable_by(@unprivileged_user, in: @repository)
    end
  end
end
